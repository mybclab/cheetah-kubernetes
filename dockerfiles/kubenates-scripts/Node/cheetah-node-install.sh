#! /bin/bash

#초기 업데이트
apt-get update -y

#기본 패키지 설치 및 업데이트
apt-get install apt-transport-https -y
apt-get install apt-transport-https -y
apt-get install ca-certificates -y
apt-get install curl -y

apt-get install software-properties-common -y
  
#docker key값 추가
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add 
apt-key fingerprint 0EBFCD88

#docker repo 추가
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
apt-get update -y

#docker-ce 설치
apt-get install docker-ce -y

# 도커 서비스 실행 및 추가
systemctl start docker
systemctl enable docker

#swap 끄기
swapoff -a

#쿠버네티스 키 가져오기
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add
#쿠버네티스 repo 추가
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" >> /etc/apt/sources.list.d/kubernetes.list
apt-get update -y
#쿠버네티스 패키지 설치
apt-get install -y kubelet kubeadm kubectl kubernetes-cni -y

#파라미터 출력
echo "master IP: $1" 
echo "token: $2"
echo "sha256: $3"

#쿠버네티스 join
kubeadm join $1:6443 --token $2 --discovery-token-ca-cert-hash sha256:$3

echo "kubeadm join " + $1 + ":6443 --token" + $2 + "--discovery-token-ca-cert-hash sha256:" + $3

#치타에 호스트 이름 보내기 
hostname=`hostname`
echo $hostname
curl -X POST http://cheetah.n3ncloud.co.kr/kube/node/$hostname/update

