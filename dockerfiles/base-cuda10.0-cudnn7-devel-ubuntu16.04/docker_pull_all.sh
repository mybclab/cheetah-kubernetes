#!/bin/bash

docker pull registry.mbl.com:5000/cheetah-base:cuda10.0-cudnn7-runtime-ubuntu16.04


docker pull registry.mbl.com:5000/cheetah-jupyter:ubuntu16.04-jupyter5.7.5


docker pull registry.mbl.com:5000/cheetah-jupyter-tensorflow:ubuntu16.04-jupyter5.7.5-tensorflow1.12


docker pull registry.mbl.com:5000/cheetah-jupyter-tensorflow-keras:ubuntu16.04-jupyter5.7.5-tensorflow1.12-keras2.2

docker pull registry.mbl.com:5000/cheetah-jupyter-tensorflow-pytorch:ubuntu16.04-jupyter5.7.5-tensorflow1.12-pytorch1.0