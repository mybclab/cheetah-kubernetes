#!/bin/bash

docker tag cheetah-base:cuda10.0-cudnn7-runtime-ubuntu16.04 registry.mbl.com:5000/cheetah-base:cuda10.0-cudnn7-runtime-ubuntu16.04
docker push registry.mbl.com:5000/cheetah-base:cuda10.0-cudnn7-runtime-ubuntu16.04


docker tag cheetah-jupyter:ubuntu16.04-jupyter5.7.5 registry.mbl.com:5000/cheetah-jupyter:ubuntu16.04-jupyter5.7.5
docker push registry.mbl.com:5000/cheetah-jupyter:ubuntu16.04-jupyter5.7.5


docker tag cheetah-jupyter-tensorflow:ubuntu16.04-jupyter5.7.5-tensorflow1.12 registry.mbl.com:5000/cheetah-jupyter-tensorflow:ubuntu16.04-jupyter5.7.5-tensorflow1.12
docker push registry.mbl.com:5000/cheetah-jupyter-tensorflow:ubuntu16.04-jupyter5.7.5-tensorflow1.12


docker tag cheetah-jupyter-tensorflow-keras:ubuntu16.04-jupyter5.7.5-tensorflow1.12-keras2.2 registry.mbl.com:5000/cheetah-jupyter-tensorflow-keras:ubuntu16.04-jupyter5.7.5-tensorflow1.12-keras2.2
docker push registry.mbl.com:5000/cheetah-jupyter-tensorflow-keras:ubuntu16.04-jupyter5.7.5-tensorflow1.12-keras2.2

docker tag cheetah-jupyter-tensorflow-pytorch:ubuntu16.04-jupyter5.7.5-tensorflow1.12-pytorch1.0 registry.mbl.com:5000/cheetah-jupyter-tensorflow-pytorch:ubuntu16.04-jupyter5.7.5-tensorflow1.12-pytorch1.0
docker push registry.mbl.com:5000/cheetah-jupyter-tensorflow-pytorch:ubuntu16.04-jupyter5.7.5-tensorflow1.12-pytorch1.0