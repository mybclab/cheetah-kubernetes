
ALTER DATABASE cheetah DEFAULT CHARACTER SET utf8;
GRANT ALL PRIVILEGES ON cheetah.* TO 'cheetah'@'%' IDENTIFIED BY 'cheetah!@#';
FLUSH PRIVILEGES;

USE cheetah;

-- 사용자
CREATE TABLE USER (
	USERNAME               VARCHAR(20)  NOT NULL, -- 사용자아이디
	PARENT_USERNAME        VARCHAR(20)  NULL,     -- 상위 사용자 아이디
	GROUP_NAME             VARCHAR(50)  NULL,     -- 그룹 명
	GROUP_TYPE_CODE        VARCHAR(50)  NULL,     -- 그룹 구분 코드
	GROUP_INVITE_KEY             VARCHAR(100) NULL,     -- 그룹 초대 키
	GROUP_IMAGE_FILE_SN          BIGINT       NULL,     -- 그룹 이미지 파일 일련번호
	NAME                   VARCHAR(50)  NOT NULL, -- 이름
	PASSWORD               VARCHAR(255) NOT NULL, -- 비밀번호
	PHONE_NO               VARCHAR(30)  NOT NULL, -- 전화 번호
	EMAIL                  VARCHAR(40)  NOT NULL, -- 이메일
	CONFIRM_YN             BOOLEAN      NOT NULL DEFAULT 0, -- 승인 여부
	SLACK_HOOK_URL               VARCHAR(500) NULL,     -- 슬랙 훅 URL
	SLACK_CHANNEL                VARCHAR(100) NULL,     -- 슬랙 채널
	USER_IMAGE_FILE_SN          BIGINT       NULL,     -- 사용자 이미지 파일 일련번호
	ORGANIZATION_NAME            VARCHAR(100) NULL,     -- 단체 명
	TEAM_NAME                    VARCHAR(100) NULL,     -- 팀 명
	CREATED_AT             DATETIME         NOT NULL,  -- 등록일시
	CREATED_BY             VARCHAR(20)  NULL, -- 등록사용자
	UPDATED_AT             DATETIME         NULL,     -- 수정일시
	UPDATED_BY             VARCHAR(20)  NULL,      -- 수정사용자
	WITHDRAWAL_DATE        DATETIME NULL      -- 탈퇴 일시
);

-- 사용자
ALTER TABLE USER
	ADD CONSTRAINT PK_USER -- 사용자 기본키
		PRIMARY KEY (
			USERNAME -- 사용자아이디
		);

-- 사용자
ALTER TABLE USER
	ADD CONSTRAINT FK_USER_TO_USER -- 사용자 -> 사용자
		FOREIGN KEY (
			PARENT_USERNAME -- 상위 사용자 아이디
		)
		REFERENCES USER ( -- 사용자
			USERNAME -- 사용자아이디
		);

-- 사용자 권한
CREATE TABLE USER_ROLE (
	USERNAME  VARCHAR(20) NOT NULL, -- 사용자아이디
	ROLE_CODE VARCHAR(50) NOT NULL  -- 권한 코드
);

-- 사용자 권한
ALTER TABLE USER_ROLE
	ADD CONSTRAINT PK_USER_ROLE -- 사용자 권한 기본키
		PRIMARY KEY (
			USERNAME,  -- 사용자아이디
			ROLE_CODE  -- 권한 코드
		);

-- 사용자 권한
ALTER TABLE USER_ROLE
	ADD CONSTRAINT FK_USER_TO_USER_ROLE -- 사용자 -> 사용자 권한
		FOREIGN KEY (
			USERNAME -- 사용자아이디
		)
		REFERENCES USER ( -- 사용자
			USERNAME -- 사용자아이디
		);


-- 코드 그룹
CREATE TABLE CODE_GROUP (
	CODE_GROUP_ID   VARCHAR(2)  NOT NULL, -- 코드 그룹 아이디
	CODE_GROUP_NAME VARCHAR(50) NOT NULL  -- 코드 그룹 이름
);

-- 코드 그룹
ALTER TABLE CODE_GROUP
	ADD CONSTRAINT PK_CODE_GROUP -- 코드 그룹 기본키
		PRIMARY KEY (
			CODE_GROUP_ID -- 코드 그룹 아이디
		);


-- 코드
CREATE TABLE CODE (
	CODE_GROUP_ID    VARCHAR(2)    NOT NULL, -- 코드 그룹 아이디
	CODE             VARCHAR(50)   NOT NULL, -- 코드
	CODE_NAME        VARCHAR(50)   NOT NULL, -- 코드 이름
	CODE_DESCRIPTION VARCHAR(2000) NULL,     -- 코드 설명
	ODR              INTEGER(3)    NOT NULL DEFAULT 1, -- 순서
	ACTIVE_YN        BOOLEAN       NOT NULL DEFAULT 0 -- 활성 여부
);

-- 코드
ALTER TABLE CODE
	ADD CONSTRAINT PK_CODE -- 코드 기본키
		PRIMARY KEY (
			CODE_GROUP_ID, -- 코드 그룹 아이디
			CODE           -- 코드
		);

-- 코드
ALTER TABLE CODE
	ADD CONSTRAINT FK_CODE_GROUP_TO_CODE -- 코드 그룹 -> 코드
		FOREIGN KEY (
			CODE_GROUP_ID -- 코드 그룹 아이디
		)
		REFERENCES CODE_GROUP ( -- 코드 그룹
			CODE_GROUP_ID -- 코드 그룹 아이디
		);


-- GPU
CREATE TABLE GPU (
	GPU_SN            BIGINT       NOT NULL, -- GPU 일련번호
	CLOUD_TYPE                 VARCHAR(50)  NOT NULL DEFAULT 'ON_PREMISE', -- 클라우드 구분
	RESOURCE_TYPE              VARCHAR(50)  NOT NULL DEFAULT 'GPU', -- 자원 구분
	GPU_NAME          VARCHAR(100) NOT NULL, -- GPU 명
	GPU_LABEL         VARCHAR(100) NULL,     -- GPU 라벨
	MANUFACTURER_NAME VARCHAR(100) NOT NULL, -- 제조사 명
	SPEC              VARCHAR(500) NOT NULL, -- 사양
	MINUTELY_AMOUNT            DOUBLE       NOT NULL DEFAULT 0, -- 분단위 금액
	HOURLY_AMOUNT             DOUBLE       NOT NULL DEFAULT 0, -- 시간단위 금액
	DAILY_AMOUNT              DOUBLE       NOT NULL DEFAULT 0, -- 일단위 금액
	MONTHLY_AMOUNT            DOUBLE       NOT NULL DEFAULT 0, -- 월단위 금액
	MONTHLY_CONTRACT_AMOUNT   DOUBLE       NOT NULL DEFAULT 0, -- 월단위 약정 금액
	MONTHLY_CONTRACT_AMOUNT_3 DOUBLE       NOT NULL DEFAULT 0, -- 월단위 약정 금액 3
	MONTHLY_CONTRACT_AMOUNT_6 DOUBLE       NOT NULL DEFAULT 0, -- 월단위 약정 금액 6
	MONTHLY_CONTRACT_AMOUNT_12    DOUBLE       NOT NULL DEFAULT 0, -- 월단위 약정 금액 12
	MONTHLY_CONTRACT_AMOUNT_24  DOUBLE       NOT NULL DEFAULT 0, -- 월단위 약정 금액 24
	QUANTITY_FORMAT            VARCHAR(100) NOT NULL, -- 수량 구성
	SHARED_YN                  BOOLEAN      NOT NULL DEFAULT 1, -- 공유 여부
	USE_YN                     BOOLEAN      NOT NULL DEFAULT 1, -- 사용 여부
	GPU_MEMORY                 INTEGER      NULL,     -- GPU 메모리
	CPU_CORE                   INTEGER      NULL,     -- CPU 코어
	SYSTEM_MEMORY              INTEGER      NULL,     -- 시스템 메모리
	CREATED_AT        DATETIME         NULL,     -- 등록일시
	CREATED_BY        VARCHAR(20)  NULL,     -- 등록사용자
	UPDATED_AT        DATETIME         NULL,     -- 수정일시
	UPDATED_BY        VARCHAR(20)  NULL      -- 수정사용자
);

-- GPU
ALTER TABLE GPU
	ADD CONSTRAINT PK_GPU -- GPU 기본키
		PRIMARY KEY (
			GPU_SN -- GPU 일련번호
		);

ALTER TABLE GPU
	MODIFY COLUMN GPU_SN BIGINT NOT NULL AUTO_INCREMENT;


-- 노드
CREATE TABLE NODE (
	NODE_SN         BIGINT       NOT NULL, -- 노드 일련번호
	NODE_NAME       VARCHAR(100) NOT NULL, -- 노드 명
	INTERNAL_DOMAIN VARCHAR(255) NULL,     -- 내부 도메인
	INTERNAL_IP     VARCHAR(15)  NULL,     -- 내부 아이피
	EXTERNAL_IP     VARCHAR(15)  NULL,     -- 외부 아이피
	STATUS_CODE     VARCHAR(50)  NOT NULL, -- 상태 코드
	GPU_LABEL       VARCHAR(100) NULL,     -- GPU 라벨
	GPU_QUANTITY    INTEGER(4)   NULL     DEFAULT 0, -- GPU 수량
	MASTER_YN       BOOLEAN      NULL     DEFAULT 0, -- 마스터 여부
	CREATED_AT      DATETIME         NULL,     -- 등록일시
	CREATED_BY      VARCHAR(20)  NULL,     -- 등록사용자
	UPDATED_AT      DATETIME         NULL,     -- 수정일시
	UPDATED_BY      VARCHAR(20)  NULL      -- 수정사용자
);

-- 노드
ALTER TABLE NODE
	ADD CONSTRAINT PK_NODE -- 노드 기본키
		PRIMARY KEY (
			NODE_SN -- 노드 일련번호
		);

ALTER TABLE NODE
	MODIFY COLUMN NODE_SN BIGINT NOT NULL AUTO_INCREMENT;


-- 파드 구성
CREATE TABLE POD_FORMAT (
	POD_FORMAT_SN          BIGINT        NOT NULL, -- 파드 구성 일련번호
	POD_FORMAT_NAME        VARCHAR(100)  NOT NULL, -- 파드 구성 명
	POD_OPTION_HINT        VARCHAR(500) NULL,     -- 파드 옵션 힌트
	POD_FORMAT_DESCRIPTION VARCHAR(500)  NULL,     -- 파드 구성 설명
	IMAGE                  VARCHAR(500)  NOT NULL, -- 이미지
	COMMAND                VARCHAR(500)  NULL,     -- 명령어
	CAPACITY               VARCHAR(10)   NOT NULL, -- 용량
	USE_YN                 BOOLEAN       NOT NULL DEFAULT 1, -- 사용 여부
  ODR                    INTEGER(3)   NULL     DEFAULT 1, -- 순서
	CREATED_AT             DATETIME      NULL,     -- 등록일시
	CREATED_BY             VARCHAR(20)   NULL,     -- 등록사용자
	UPDATED_AT             DATETIME      NULL,     -- 수정일시
	UPDATED_BY             VARCHAR(20)   NULL      -- 수정사용자
);

-- 파드 구성
ALTER TABLE POD_FORMAT
	ADD CONSTRAINT PK_POD_FORMAT -- 파드 구성 기본키
		PRIMARY KEY (
			POD_FORMAT_SN -- 파드 구성 일련번호
		);
ALTER TABLE POD_FORMAT
	MODIFY COLUMN POD_FORMAT_SN BIGINT NOT NULL AUTO_INCREMENT;


-- 컨테이너
CREATE TABLE CONTAINER (
	USERNAME        VARCHAR(20)  NOT NULL, -- 사용자아이디
	CONTAINER_ID    VARCHAR(20)  NOT NULL, -- 컨테이너 아이디
	CONTAINER_NAME  VARCHAR(100) NOT NULL, -- 컨테이너 명
	GPU_SN          BIGINT       NOT NULL, -- GPU 일련번호
	NODE_SN         BIGINT       NULL,     -- 노드 일련번호
	IMAGE_TYPE_CODE      VARCHAR(50)   NOT NULL, -- 이미지 구분 코드
	POD_FORMAT_SN   BIGINT       NULL,     -- 파드 구성 일련번호
	CUSTOM_IMAGE_SN      BIGINT        NULL,     -- 커스텀 이미지 일련번호
	GPU_QUANTITY    INTEGER(4)   NOT NULL DEFAULT 0, -- GPU 수량
	INTERNAL_IP     VARCHAR(15)  NULL,     -- 내부 아이피
	EXTERNAL_IP     VARCHAR(15)  NULL,     -- 외부 아이피
	NOTEBOOK_PORT   INTEGER(5)   NULL,     -- 노트북 포트
	NOTEBOOK_TOKEN  VARCHAR(100) NULL,     -- 노트북 토큰
	SSH_PORT        INTEGER(5)   NULL,     -- SSH 포트
	SSH_PASSWORD    VARCHAR(100) NULL,     -- SSH 비밀번호
	WEBSOCKET_PORT  INTEGER(5)   NULL,     -- 웹소켓 포트
	TENSORBOARD_PORT INTEGER(5)   NULL,     -- 텐서보드 포트
	DESCRIPTION     VARCHAR(100) NOT NULL, -- 설명
	STATUS_CODE     VARCHAR(50)  NULL,     -- 상태 코드
	POD_NAME        VARCHAR(100) NULL,     -- 파드 명
	POD_STATUS_CODE VARCHAR(50)  NULL,     -- 파드 상태 코드
	START_DATE      DATETIME         NULL,     -- 시작 일시
	RETURN_DATE     DATETIME         NULL,     -- 반납 일시
	CHARGING_METHOD_CODE VARCHAR(50)  NULL,     -- 과금 방법 코드
	INSTANCE_ID          VARCHAR(100)  NULL,     -- 인스턴스 아이디
	LISTEN_YN            BOOLEAN       NULL     DEFAULT 1, -- LISTEN_여부
	PARENT_CONTAINER_ID  VARCHAR(20)   NULL,     -- 상위 컨테이너 아이디
	GROUP_SHARED_YN      BOOLEAN       NULL     DEFAULT 0, -- 그룹 공유 여부
	GROUP_SHARED_SIZE       DOUBLE       NULL,     -- 그룹 공유 크기
	GROUP_TARGET_USER    VARCHAR(1000) NULL,     -- 그룹 대상 사용자
	CPU_LIMIT            INTEGER       NULL,     -- CPU 제한
	MEMORY_LIMIT         INTEGER       NULL,     -- 메모리 제한
	TOKEN_YN            BOOLEAN       NULL     DEFAULT 1, -- 토큰 여부
	BASE_PATH          VARCHAR(500) NULL,     -- 기초 경로
	CREATED_AT      DATETIME         NULL,     -- 등록일시
	CREATED_BY      VARCHAR(20)  NULL,     -- 등록사용자
	UPDATED_AT      DATETIME         NULL,     -- 수정일시
	UPDATED_BY      VARCHAR(20)  NULL      -- 수정사용자
);

-- 컨테이너
ALTER TABLE CONTAINER
	ADD CONSTRAINT PK_CONTAINER -- 컨테이너 기본키
		PRIMARY KEY (
			USERNAME,     -- 사용자아이디
			CONTAINER_ID  -- 컨테이너 아이디
		);


-- 컨테이너
ALTER TABLE CONTAINER
	ADD CONSTRAINT FK_GPU_TO_CONTAINER -- GPU -> 컨테이너
		FOREIGN KEY (
			GPU_SN -- GPU 일련번호
		)
		REFERENCES GPU ( -- GPU
			GPU_SN -- GPU 일련번호
		);

-- 컨테이너
ALTER TABLE CONTAINER
	ADD CONSTRAINT FK_NODE_TO_CONTAINER -- 노드 -> 컨테이너
		FOREIGN KEY (
			NODE_SN -- 노드 일련번호
		)
		REFERENCES NODE ( -- 노드
			NODE_SN -- 노드 일련번호
		);

-- 컨테이너
ALTER TABLE CONTAINER
	ADD CONSTRAINT FK_USER_TO_CONTAINER -- 사용자 -> 컨테이너
		FOREIGN KEY (
			USERNAME -- 사용자아이디
		)
		REFERENCES USER ( -- 사용자
			USERNAME -- 사용자아이디
		);




-- 파드 컨디션
CREATE TABLE POD_CONDITION (
	POD_CONDITION_SN     BIGINT       NOT NULL, -- 파드 컨디션 일련번호
	USERNAME             VARCHAR(20)  NOT NULL, -- 사용자아이디
	CONTAINER_ID         VARCHAR(20)  NOT NULL, -- 컨테이너 아이디
	POD_NAME             VARCHAR(100) NOT NULL, -- 파드 명
	LAST_PROBE_TIME      DATETIME     NULL,     -- 마지막 조사 일시
	LAST_TRANSITION_TIME DATETIME     NULL,     -- 마지막 전이 일시
	MESSAGE              VARCHAR(500) NULL,     -- 메세지
	REASON               VARCHAR(100) NULL,     -- 이유
	STATUS               BOOLEAN      NULL, -- 상태 코드
	TYPE_CODE            VARCHAR(50)  NULL,     -- 타입 코드
	CREATED_AT           DATETIME     NOT NULL  -- 등록일시
);

-- 파드 컨디션
ALTER TABLE POD_CONDITION
	ADD CONSTRAINT PK_POD_CONDITION -- 파드 컨디션 기본키
		PRIMARY KEY (
			POD_CONDITION_SN -- 파드 컨디션 일련번호
		);

ALTER TABLE POD_CONDITION
	MODIFY COLUMN POD_CONDITION_SN BIGINT NOT NULL AUTO_INCREMENT;

-- 파드 컨디션
ALTER TABLE POD_CONDITION
	ADD CONSTRAINT FK_CONTAINER_TO_POD_CONDITION -- 컨테이너 -> 파드 컨디션
		FOREIGN KEY (
			USERNAME,     -- 사용자아이디
			CONTAINER_ID  -- 컨테이너 아이디
		)
		REFERENCES CONTAINER ( -- 컨테이너
			USERNAME,     -- 사용자아이디
			CONTAINER_ID  -- 컨테이너 아이디
		);


-- 크레딧
CREATE TABLE CREDIT (
	CREDIT_ID               VARCHAR(50) NOT NULL, -- 크레딧 아이디
	CREDIT_NAME             VARCHAR(50) NOT NULL, -- 크레딧 명
	CREDIT_TYPE_CODE        VARCHAR(50) NOT NULL, -- 크레딧 구분 코드
	CREDIT_AMOUNT           DOUBLE      NOT NULL DEFAULT 0, -- 크레딧 금액
	CREDIT_EXPIRY_TYPE_CODE VARCHAR(50) NOT NULL, -- 크레딧 유효기간 구분 코드
	CREDIT_EXPIRY_VALUE     INTEGER     NULL,     -- 크레딧 유효기간 값
	CREATED_AT              DATETIME        NULL,     -- 등록일시
	CREATED_BY              VARCHAR(20) NULL,     -- 등록사용자
	UPDATED_AT              DATETIME        NULL,     -- 수정일시
	UPDATED_BY              VARCHAR(20) NULL      -- 수정사용자
);

-- 크레딧
ALTER TABLE CREDIT
	ADD CONSTRAINT PK_CREDIT -- 크레딧 기본키
		PRIMARY KEY (
			CREDIT_ID -- 크레딧 아이디
		);


-- 사용자 크레딧
CREATE TABLE USER_CREDIT (
	USERNAME             VARCHAR(20) NOT NULL, -- 사용자아이디
	USER_CREDIT_NO       INTEGER(3)  NOT NULL DEFAULT 0, -- 사용자 크레딧 번호
	CREDIT_ID            VARCHAR(50) NOT NULL, -- 크레딧 아이디
	CREDIT_AMOUNT        DOUBLE      NOT NULL DEFAULT 0, -- 크레딧 금액
	REMAIN_CREDIT_AMOUNT DOUBLE      NOT NULL DEFAULT 0, -- 잔여 크레딧 금액
	EXPIRY_START_DATE    VARCHAR(8)  NULL,     -- 유효기간 시작 일시
	EXPIRY_END_DATE      VARCHAR(8)  NULL,     -- 유효기간 종료 일시
	CREATED_AT           DATETIME    NULL,     -- 등록일시
	CREATED_BY           VARCHAR(20) NULL,     -- 등록사용자
	UPDATED_AT           DATETIME    NULL,     -- 수정일시
	UPDATED_BY           VARCHAR(20) NULL      -- 수정사용자
);

-- 사용자 크레딧
ALTER TABLE USER_CREDIT
	ADD CONSTRAINT PK_USER_CREDIT -- 사용자 크레딧 기본키
		PRIMARY KEY (
			USERNAME,       -- 사용자아이디
			USER_CREDIT_NO  -- 사용자 크레딧 번호
		);

-- 사용자 크레딧
ALTER TABLE USER_CREDIT
	ADD CONSTRAINT FK_USER_TO_USER_CREDIT -- 사용자 -> 사용자 크레딧
		FOREIGN KEY (
			USERNAME -- 사용자아이디
		)
		REFERENCES USER ( -- 사용자
			USERNAME -- 사용자아이디
		);

-- 사용자 크레딧
ALTER TABLE USER_CREDIT
	ADD CONSTRAINT FK_CREDIT_TO_USER_CREDIT -- 크레딧 -> 사용자 크레딧
		FOREIGN KEY (
			CREDIT_ID -- 크레딧 아이디
		)
		REFERENCES CREDIT ( -- 크레딧
			CREDIT_ID -- 크레딧 아이디
		);


-- 사용자 카드
CREATE TABLE USER_CARD (
	USERNAME     VARCHAR(20)   NOT NULL, -- 사용자아이디
	USER_CARD_NO INTEGER(3)    NOT NULL DEFAULT 0, -- 사용자 카드 번호
	CARD_NO      VARCHAR(19)   NOT NULL, -- 카드 번호
	CARD_EXPIRY  VARCHAR(4)    NOT NULL, -- 카드 유효기간
	CUSTOMER_UID  VARCHAR(1000) NOT NULL, -- 빌링 키
	USE_YN       BOOLEAN       NOT NULL DEFAULT 1, -- 사용 여부
	CREATED_AT   DATETIME      NULL,     -- 등록일시
	CREATED_BY   VARCHAR(20)   NULL,     -- 등록사용자
	UPDATED_AT   DATETIME      NULL,     -- 수정일시
	UPDATED_BY   VARCHAR(20)   NULL      -- 수정사용자
);

-- 사용자 카드
ALTER TABLE USER_CARD
	ADD CONSTRAINT PK_USER_CARD -- 사용자 카드 기본키
		PRIMARY KEY (
			USERNAME,     -- 사용자아이디
			USER_CARD_NO  -- 사용자 카드 번호
		);

-- 사용자 카드
ALTER TABLE USER_CARD
	ADD CONSTRAINT FK_USER_TO_USER_CARD -- 사용자 -> 사용자 카드
		FOREIGN KEY (
			USERNAME -- 사용자아이디
		)
		REFERENCES USER ( -- 사용자
			USERNAME -- 사용자아이디
		);


-- 결제
CREATE TABLE PAYMENT (
	USERNAME              VARCHAR(20) NOT NULL, -- 사용자아이디
	PAYMENT_ID            VARCHAR(50) NOT NULL, -- 결제 아이디
	TARGET_YEAR_MONTH     VARCHAR(6)  NOT NULL, -- 대상년월
	USE_START_DATE        VARCHAR(8)  NOT NULL, -- 이용 시작 일시
	USE_END_DATE          VARCHAR(8)  NOT NULL, -- 이용 종료 일시
	TOTAL_PAYMENT_AMOUNT  DOUBLE      NOT NULL DEFAULT 0, -- 총 결제 금액
	CREDIT_PAYMENT_AMOUNT DOUBLE      NOT NULL DEFAULT 0, -- 크레딧 결제 금액
	CARD_PAYMENT_AMOUNT   DOUBLE      NOT NULL DEFAULT 0, -- 카드 결제 금액
	USER_CARD_NO          INTEGER(3)  NULL,     -- 사용자 카드 번호
	USER_CREDIT_NO        INTEGER(3)  NULL     DEFAULT 0, -- 사용자 크레딧 번호
	USE_QUANTITY          INTEGER(4)  NOT NULL DEFAULT 0, -- 이용 수량
	PAYMENT_STATUS_CODE   VARCHAR(50) NOT NULL, -- 결제 상태 코드
	PAYMENT_DATE          DATETIME    NOT NULL  -- 결제 일시
);

-- 결제
ALTER TABLE PAYMENT
	ADD CONSTRAINT PK_PAYMENT -- 결제 기본키
		PRIMARY KEY (
			USERNAME,   -- 사용자아이디
			PAYMENT_ID  -- 결제 아이디
		);

-- 결제
ALTER TABLE PAYMENT
	ADD CONSTRAINT FK_USER_TO_PAYMENT -- 사용자 -> 결제
		FOREIGN KEY (
			USERNAME -- 사용자아이디
		)
		REFERENCES USER ( -- 사용자
			USERNAME -- 사용자아이디
		);


-- 사용자 카드 결제
CREATE TABLE USER_CARD_PAYMENT (
	PAYMENT_ID          VARCHAR(50)  NOT NULL, -- 결제 아이디
	USERNAME            VARCHAR(20)  NOT NULL, -- 사용자아이디
	USER_CARD_NO        INTEGER(3)   NOT NULL DEFAULT 0, -- 사용자 카드 번호
	TRANSACTION_PG      VARCHAR(50)  NOT NULL, -- 거래 PG
	TRANSACTION_UID     VARCHAR(200) NOT NULL, -- 거래 고유번호
	CARD_APPLY_NO       VARCHAR(200) NOT NULL, -- 카드 승인 번호
	PAYMENT_AMOUNT      DOUBLE       NOT NULL DEFAULT 0, -- 결제 금액
	PAYMENT_STATUS_CODE VARCHAR(50)  NOT NULL, -- 결제 상태 코드
	PAYMENT_DATE        DATETIME     NOT NULL  -- 결제 일시
);

-- 사용자 카드 결제
ALTER TABLE USER_CARD_PAYMENT
	ADD CONSTRAINT PK_USER_CARD_PAYMENT -- 사용자 카드 결제 기본키
		PRIMARY KEY (
			PAYMENT_ID,   -- 결제 아이디
			USERNAME,     -- 사용자아이디
			USER_CARD_NO  -- 사용자 카드 번호
		);

-- 사용자 카드 결제
ALTER TABLE USER_CARD_PAYMENT
	ADD CONSTRAINT FK_USER_CARD_TO_USER_CARD_PAYMENT -- 사용자 카드 -> 사용자 카드 결제
		FOREIGN KEY (
			USERNAME,     -- 사용자아이디
			USER_CARD_NO  -- 사용자 카드 번호
		)
		REFERENCES USER_CARD ( -- 사용자 카드
			USERNAME,     -- 사용자아이디
			USER_CARD_NO  -- 사용자 카드 번호
		);

-- 사용자 카드 결제
ALTER TABLE USER_CARD_PAYMENT
	ADD CONSTRAINT FK_PAYMENT_TO_USER_CARD_PAYMENT -- 결제 -> 사용자 카드 결제
		FOREIGN KEY (
			USERNAME,   -- 사용자아이디
			PAYMENT_ID  -- 결제 아이디
		)
		REFERENCES PAYMENT ( -- 결제
			USERNAME,   -- 사용자아이디
			PAYMENT_ID  -- 결제 아이디
		);


-- 사용자 크레딧 결제
CREATE TABLE USER_CREDIT_PAYMENT (
	PAYMENT_ID             VARCHAR(50) NOT NULL, -- 결제 아이디
	USERNAME               VARCHAR(20) NOT NULL, -- 사용자아이디
	USER_CREDIT_NO         INTEGER(3)  NOT NULL DEFAULT 0, -- 사용자 크레딧 번호
	CREDIT_PAYMENT_AMOUNT  DOUBLE      NOT NULL DEFAULT 0, -- 크레딧 결제 금액
	CREDIT_PREVIOUS_AMOUNT DOUBLE      NOT NULL DEFAULT 0, -- 크레딧 이전 금액
	CREDIT_REMAIN_AMOUNT   DOUBLE      NOT NULL DEFAULT 0, -- 크레딧 잔여 금액
	PAYMENT_DATE           DATETIME    NOT NULL  -- 결제 일시
);

-- 사용자 크레딧 결제
ALTER TABLE USER_CREDIT_PAYMENT
	ADD CONSTRAINT PK_USER_CREDIT_PAYMENT -- 사용자 크레딧 결제 기본키
		PRIMARY KEY (
			PAYMENT_ID,     -- 결제 아이디
			USERNAME,       -- 사용자아이디
			USER_CREDIT_NO  -- 사용자 크레딧 번호
		);

-- 사용자 크레딧 결제
ALTER TABLE USER_CREDIT_PAYMENT
	ADD CONSTRAINT FK_USER_CREDIT_TO_USER_CREDIT_PAYMENT -- 사용자 크레딧 -> 사용자 크레딧 결제
		FOREIGN KEY (
			USERNAME,       -- 사용자아이디
			USER_CREDIT_NO  -- 사용자 크레딧 번호
		)
		REFERENCES USER_CREDIT ( -- 사용자 크레딧
			USERNAME,       -- 사용자아이디
			USER_CREDIT_NO  -- 사용자 크레딧 번호
		);

-- 사용자 크레딧 결제
ALTER TABLE USER_CREDIT_PAYMENT
	ADD CONSTRAINT FK_PAYMENT_TO_USER_CREDIT_PAYMENT -- 결제 -> 사용자 크레딧 결제
		FOREIGN KEY (
			USERNAME,   -- 사용자아이디
			PAYMENT_ID  -- 결제 아이디
		)
		REFERENCES PAYMENT ( -- 결제
			USERNAME,   -- 사용자아이디
			PAYMENT_ID  -- 결제 아이디
		);


-- 쿠버네티스 YAML
CREATE TABLE KUBERNETES_YAML (
	KIND VARCHAR(100)  NOT NULL, -- KIND
	YAML VARCHAR(4000) NULL      -- YAML
);

-- 쿠버네티스 YAML
ALTER TABLE KUBERNETES_YAML
	ADD CONSTRAINT PK_KUBERNETES_YAML -- 쿠버네티스 YAML 기본키
		PRIMARY KEY (
			KIND -- KIND
		);


-- 게시물
CREATE TABLE POST (
	POST_SN    BIGINT        NOT NULL, -- 게시물 일련번호
	BOARD_TYPE_CODE VARCHAR(50)   NOT NULL, -- 게시판 구분
	TITLE      VARCHAR(200)  NOT NULL, -- 제목
	CONTENTS   VARCHAR(2000) NOT NULL, -- 내용
	USE_YN     BOOLEAN       NOT NULL DEFAULT 1, -- 사용 여부
	READ_COUNT INTEGER(5)    NOT NULL DEFAULT 0, -- 조회 수
	SCORE DOUBLE(20,10) NULL,
	HIDDEN_SCORE DOUBLE(20,10) NULL,
	CREATED_AT DATETIME      NULL,     -- 등록일시
	CREATED_BY VARCHAR(20)   NULL,     -- 등록사용자
	UPDATED_AT DATETIME      NULL,     -- 수정일시
	UPDATED_BY VARCHAR(20)   NULL,     -- 수정사용자
	GROUP_ID VARCHAR(20)   NULL     -- 그룹 아이디
);

-- 게시물
ALTER TABLE POST
	ADD CONSTRAINT PK_POST -- 게시물 기본키
		PRIMARY KEY (
			POST_SN -- 게시물 일련번호
		);

ALTER TABLE POST
	MODIFY COLUMN POST_SN BIGINT NOT NULL AUTO_INCREMENT;


-- 게시물 댓글
CREATE TABLE POST_COMMENT (
	POST_SN    BIGINT        NOT NULL, -- 게시물 일련번호
	GROUP_ID        VARCHAR(20)   NULL,     -- 그룹 아이디
	POST_COMMENT_NO INTEGER(3)    NOT NULL DEFAULT 0, -- 댓글 번호
	CONTENTS   VARCHAR(2000) NOT NULL, -- 내용
	USE_YN     BOOLEAN       NOT NULL DEFAULT 1, -- 사용 여부
	CREATED_AT DATETIME      NULL,     -- 등록일시
	CREATED_BY VARCHAR(20)   NULL,     -- 등록사용자
	UPDATED_AT DATETIME      NULL,     -- 수정일시
	UPDATED_BY VARCHAR(20)   NULL      -- 수정사용자
);

-- 게시물 댓글
ALTER TABLE POST_COMMENT
	ADD CONSTRAINT PK_POST_COMMENT -- 게시물 댓글 기본키
		PRIMARY KEY (
			POST_SN,    -- 게시물 일련번호
			POST_COMMENT_NO  -- 댓글 번호
		);

-- 게시물 댓글
ALTER TABLE POST_COMMENT
	ADD CONSTRAINT FK_POST_TO_POST_COMMENT -- 게시물 -> 게시물 댓글
		FOREIGN KEY (
			POST_SN -- 게시물 일련번호
		)
		REFERENCES POST ( -- 게시물
			POST_SN -- 게시물 일련번호
		);


-- 파드 옵션 그룹
CREATE TABLE POD_OPTION_GROUP (
	POD_OPTION_GROUP_ID        VARCHAR(50)  NOT NULL, -- 파드 옵션 그룹 아이디
	POD_OPTION_GROUP_NAME      VARCHAR(100) NOT NULL, -- 파드 옵션 그룹 명
	POD_OPTION_GROUP_TYPE_CODE VARCHAR(50)  NOT NULL, -- 파드 옵션 그룹 구분 코드
	ODR                        INTEGER(3)   NULL     DEFAULT 1, -- 순서
	USE_YN                     BOOLEAN      NOT NULL DEFAULT 1, -- 사용 여부
	CREATED_AT                 DATETIME     NULL,     -- 등록일시
	CREATED_BY                 VARCHAR(20)  NULL,     -- 등록사용자
	UPDATED_AT                 DATETIME     NULL,     -- 수정일시
	UPDATED_BY                 VARCHAR(20)  NULL      -- 수정사용자
);

-- 파드 옵션 그룹
ALTER TABLE POD_OPTION_GROUP
	ADD CONSTRAINT PK_POD_OPTION_GROUP -- 파드 옵션 그룹 기본키
		PRIMARY KEY (
			POD_OPTION_GROUP_ID -- 파드 옵션 그룹 아이디
		);


-- 파드 옵션
CREATE TABLE POD_OPTION (
	POD_OPTION_SN       BIGINT       NOT NULL, -- 파드 옵션 일련번호
	POD_OPTION_GROUP_ID VARCHAR(50)  NOT NULL, -- 파드 옵션 그룹 아이디
	POD_OPTION_ID       VARCHAR(50)  NOT NULL, -- 파드 옵션 아이디
	POD_OPTION_NAME     VARCHAR(100) NOT NULL, -- 파드 옵션 명
	ODR                 INTEGER(3)   NOT NULL DEFAULT 1, -- 순서
	USE_YN              BOOLEAN      NOT NULL DEFAULT 1, -- 사용 여부
	CREATED_AT          DATETIME     NULL,     -- 등록일시
	CREATED_BY          VARCHAR(20)  NULL,     -- 등록사용자
	UPDATED_AT          DATETIME     NULL,     -- 수정일시
	UPDATED_BY          VARCHAR(20)  NULL      -- 수정사용자
);

-- 파드 옵션
ALTER TABLE POD_OPTION
	ADD CONSTRAINT PK_POD_OPTION -- 파드 옵션 기본키
		PRIMARY KEY (
			POD_OPTION_SN -- 파드 옵션 일련번호
		);

ALTER TABLE POD_OPTION
	MODIFY COLUMN POD_OPTION_SN BIGINT NOT NULL AUTO_INCREMENT;

-- 파드 옵션
ALTER TABLE POD_OPTION
	ADD CONSTRAINT FK_POD_OPTION_GROUP_TO_POD_OPTION -- 파드 옵션 그룹 -> 파드 옵션
		FOREIGN KEY (
			POD_OPTION_GROUP_ID -- 파드 옵션 그룹 아이디
		)
		REFERENCES POD_OPTION_GROUP ( -- 파드 옵션 그룹
			POD_OPTION_GROUP_ID -- 파드 옵션 그룹 아이디
		);


-- 파드 구성 옵션
CREATE TABLE POD_FORMAT_OPTION (
	POD_FORMAT_SN       BIGINT      NOT NULL, -- 파드 구성 일련번호
	POD_OPTION_SN       BIGINT      NOT NULL, -- 파드 옵션 일련번호
	POD_OPTION_GROUP_ID VARCHAR(50) NULL,     -- 파드 옵션 그룹 아이디
	POD_OPTION_ID       VARCHAR(50) NULL,     -- 파드 옵션 아이디
	CREATED_AT          DATETIME    NULL      -- 등록일시
);

-- 파드 구성 옵션
ALTER TABLE POD_FORMAT_OPTION
	ADD CONSTRAINT PK_POD_FORMAT_OPTION -- 파드 구성 옵션 기본키
		PRIMARY KEY (
			POD_FORMAT_SN, -- 파드 구성 일련번호
			POD_OPTION_SN  -- 파드 옵션 일련번호
		);

-- 파드 구성 옵션
ALTER TABLE POD_FORMAT_OPTION
	ADD CONSTRAINT FK_POD_FORMAT_TO_POD_FORMAT_OPTION -- 파드 구성 -> 파드 구성 옵션
		FOREIGN KEY (
			POD_FORMAT_SN -- 파드 구성 일련번호
		)
		REFERENCES POD_FORMAT ( -- 파드 구성
			POD_FORMAT_SN -- 파드 구성 일련번호
		);

-- 파드 구성 옵션
ALTER TABLE POD_FORMAT_OPTION
	ADD CONSTRAINT FK_POD_OPTION_TO_POD_FORMAT_OPTION -- 파드 옵션 -> 파드 구성 옵션
		FOREIGN KEY (
			POD_OPTION_SN -- 파드 옵션 일련번호
		)
		REFERENCES POD_OPTION ( -- 파드 옵션
			POD_OPTION_SN -- 파드 옵션 일련번호
		);


-- 파드 구성 GPU
CREATE TABLE POD_FORMAT_GPU (
	GPU_SN        BIGINT   NOT NULL, -- GPU 일련번호
	POD_FORMAT_SN BIGINT   NOT NULL, -- 파드 구성 일련번호
	CREATED_AT    DATETIME NULL      -- 등록일시
);

-- 파드 구성 GPU
ALTER TABLE POD_FORMAT_GPU
	ADD CONSTRAINT PK_POD_FORMAT_GPU -- 파드 구성 GPU 기본키
		PRIMARY KEY (
			GPU_SN,        -- GPU 일련번호
			POD_FORMAT_SN  -- 파드 구성 일련번호
		);

-- 파드 구성 GPU
ALTER TABLE POD_FORMAT_GPU
	ADD CONSTRAINT FK_GPU_TO_POD_FORMAT_GPU -- GPU -> 파드 구성 GPU
		FOREIGN KEY (
			GPU_SN -- GPU 일련번호
		)
		REFERENCES GPU ( -- GPU
			GPU_SN -- GPU 일련번호
		);

-- 파드 구성 GPU
ALTER TABLE POD_FORMAT_GPU
	ADD CONSTRAINT FK_POD_FORMAT_TO_POD_FORMAT_GPU -- 파드 구성 -> 파드 구성 GPU
		FOREIGN KEY (
			POD_FORMAT_SN -- 파드 구성 일련번호
		)
		REFERENCES POD_FORMAT ( -- 파드 구성
			POD_FORMAT_SN -- 파드 구성 일련번호
		);


-- 컨테이너 이력
CREATE TABLE CONTAINER_HISTORY (
	USERNAME             VARCHAR(20) NOT NULL, -- 사용자아이디
	CONTAINER_ID         VARCHAR(20) NOT NULL, -- 컨테이너 아이디
	CONTAINER_HISTORY_NO INTEGER(3)  NOT NULL DEFAULT 0, -- 컨테이너 이력 번호
	GPU_SN               BIGINT      NOT NULL, -- GPU 일련번호
	GPU_QUANTITY         INTEGER(4)  NOT NULL DEFAULT 0, -- GPU 수량
	GPU_AMOUNT           DOUBLE      NOT NULL DEFAULT 0, -- GPU 금액
	NODE_SN              BIGINT      NOT NULL, -- 노드 일련번호
	CHARGING_METHOD_CODE VARCHAR(50) NOT NULL, -- 과금 방법 코드
	START_DATE           DATETIME    NOT NULL, -- 시작 일시
	RETURN_DATE          DATETIME    NULL      -- 반납 일시
);

-- 컨테이너 이력
ALTER TABLE CONTAINER_HISTORY
	ADD CONSTRAINT PK_CONTAINER_HISTORY -- 컨테이너 이력 기본키
		PRIMARY KEY (
			USERNAME,             -- 사용자아이디
			CONTAINER_ID,         -- 컨테이너 아이디
			CONTAINER_HISTORY_NO  -- 컨테이너 이력 번호
		);


-- 컨테이너 이력
ALTER TABLE CONTAINER_HISTORY
	ADD CONSTRAINT FK_CONTAINER_TO_CONTAINER_HISTORY -- 컨테이너 -> 컨테이너 이력
		FOREIGN KEY (
			USERNAME,     -- 사용자아이디
			CONTAINER_ID  -- 컨테이너 아이디
		)
		REFERENCES CONTAINER ( -- 컨테이너
			USERNAME,     -- 사용자아이디
			CONTAINER_ID  -- 컨테이너 아이디
		);


-- 퍼블릭 클라우드 OFFERING
CREATE TABLE PUBLIC_CLOUD_OFFERING (
	CLOUD_TYPE       VARCHAR(50)  NOT NULL, -- 클라우드 구분
	OFFERING_ID      VARCHAR(50)  NOT NULL, -- 제공 아이디
	OFFERING_NAME    VARCHAR(100) NOT NULL, -- 제공 명
	GPU_SN           BIGINT       NOT NULL, -- GPU 일련번호
	GPU_QUANTITY     INTEGER(4)   NOT NULL DEFAULT 0, -- GPU 수량
	CPU_CORE         INTEGER      NULL,     -- CPU 코어
	SYSTEM_MEMORY    INTEGER      NULL,     -- 시스템 메모리
	GPU_MEMORY       INTEGER      NULL,     -- GPU 메모리
	GPU_TOTAL_MEMORY INTEGER      NULL,     -- GPU 총 메모리
	CREATED_AT       DATETIME     NULL,     -- 등록일시
	CREATED_BY       VARCHAR(20)  NULL,     -- 등록사용자
	UPDATED_AT       DATETIME     NULL,     -- 수정일시
	UPDATED_BY       VARCHAR(20)  NULL      -- 수정사용자
);

-- 퍼블릭 클라우드 OFFERING
ALTER TABLE PUBLIC_CLOUD_OFFERING
	ADD CONSTRAINT PK_PUBLIC_CLOUD_OFFERING -- 퍼블릭 클라우드 OFFERING 기본키
		PRIMARY KEY (
			CLOUD_TYPE,  -- 클라우드 구분
			OFFERING_ID  -- 제공 아이디
		);

-- 퍼블릭 클라우드 OFFERING
ALTER TABLE PUBLIC_CLOUD_OFFERING
	ADD CONSTRAINT FK_GPU_TO_PUBLIC_CLOUD_OFFERING -- GPU -> 퍼블릭 클라우드 OFFERING
		FOREIGN KEY (
			GPU_SN -- GPU 일련번호
		)
		REFERENCES GPU ( -- GPU
			GPU_SN -- GPU 일련번호
		);


-- 배포 컨디션
CREATE TABLE DEPLOYMENT_CONDITION (
	DEPLOYMENT_CONDITION_SN BIGINT       NOT NULL, -- 배포 컨디션 일련번호
	USERNAME                VARCHAR(20)  NULL,     -- 사용자아이디
	CONTAINER_ID            VARCHAR(20)  NULL,     -- 컨테이너 아이디
	DEPLOYMENT_NAME         VARCHAR(100) NULL,     -- 배포 명
	LAST_UPDATE_TIME         DATETIME     NULL,     -- 마지막 수정 일시
	LAST_TRANSITION_TIME    DATETIME     NULL,     -- 마지막 전이 일시
	MESSAGE                 VARCHAR(500) NULL,     -- 메세지
	REASON                  VARCHAR(100) NULL,     -- 이유
	STATUS             BOOLEAN      NULL     DEFAULT 1, -- 상태 코드
	TYPE_CODE               VARCHAR(50)  NULL,     -- 타입 코드
	CREATED_AT              DATETIME     NULL      -- 등록일시
);

-- 배포 컨디션
ALTER TABLE DEPLOYMENT_CONDITION
	ADD CONSTRAINT PK_DEPLOYMENT_CONDITION -- 배포 컨디션 기본키
		PRIMARY KEY (
			DEPLOYMENT_CONDITION_SN -- 배포 컨디션 일련번호
		);

ALTER TABLE DEPLOYMENT_CONDITION
	MODIFY COLUMN DEPLOYMENT_CONDITION_SN BIGINT NOT NULL AUTO_INCREMENT;

-- 배포 컨디션
ALTER TABLE DEPLOYMENT_CONDITION
	ADD CONSTRAINT FK_CONTAINER_TO_DEPLOYMENT_CONDITION -- 컨테이너 -> 배포 컨디션
		FOREIGN KEY (
			USERNAME,     -- 사용자아이디
			CONTAINER_ID  -- 컨테이너 아이디
		)
		REFERENCES CONTAINER ( -- 컨테이너
			USERNAME,     -- 사용자아이디
			CONTAINER_ID  -- 컨테이너 아이디
		);


-- 볼륨
CREATE TABLE VOLUME (
	VOLUME_SN         BIGINT        NOT NULL, -- 볼륨 일련번호
	USERNAME          VARCHAR(20)   NOT NULL, -- 사용자아이디
	VOLUME_NAME       VARCHAR(100)  NOT NULL, -- 볼륨 명
	REQUEST_SIZE       INTEGER       NOT NULL, -- 요청 용량
	GROUP_TARGET_USER VARCHAR(1000) NULL,     -- 그룹 대상 사용자
	DESCRIPTION       VARCHAR(100)  NULL,     -- 설명
	STATUS_CODE   VARCHAR(50)   NULL,     -- 상태 코드
	SSH_PORT          INTEGER(5)    NULL,     -- SSH 포트
	SSH_PASSWORD      VARCHAR(100)  NULL,     -- SSH 비밀번호
	NODE_SN           BIGINT        NULL,     -- 노드 일련번호
	POD_NAME          VARCHAR(100)  NULL,     -- 파드 명
	POD_STATUS_CODE   VARCHAR(50)   NULL,     -- 파드 상태 코드
	EXTERNAL_IP       VARCHAR(15)   NULL,     -- 외부 아이피
	BASE_PATH          VARCHAR(500) NULL,     -- 기초 경로
	CREATED_AT        DATETIME      NULL,     -- 등록일시
	CREATED_BY        VARCHAR(20)   NULL,     -- 등록사용자
	UPDATED_AT        DATETIME      NULL,     -- 수정일시
	UPDATED_BY        VARCHAR(20)   NULL      -- 수정사용자
);

-- 볼륨
ALTER TABLE VOLUME
	ADD CONSTRAINT PK_VOLUME -- 볼륨 기본키
		PRIMARY KEY (
			VOLUME_SN -- 볼륨 일련번호
		);

ALTER TABLE VOLUME
	MODIFY COLUMN VOLUME_SN BIGINT NOT NULL AUTO_INCREMENT;

-- 볼륨
ALTER TABLE VOLUME
	ADD CONSTRAINT FK_USER_TO_VOLUME -- 사용자 -> 볼륨
		FOREIGN KEY (
			USERNAME -- 사용자아이디
		)
		REFERENCES USER ( -- 사용자
			USERNAME -- 사용자아이디
		);

-- 컨테이너 볼륨
CREATE TABLE CONTAINER_VOLUME (
	USERNAME            VARCHAR(20)  NOT NULL, -- 사용자아이디
	CONTAINER_ID        VARCHAR(20)  NOT NULL, -- 컨테이너 아이디
	CONTAINER_VOLUME_NO INTEGER(3)   NOT NULL DEFAULT 0, -- 컨테이너 볼륨 번호
	VOLUME_SN           BIGINT       NULL,     -- 볼륨 일련번호
	CLOUD_TYPE          VARCHAR(50)  NOT NULL, -- 클라우드 구분
	VOLUME_ID           VARCHAR(100) NULL, -- 볼륨 아이디
	VOLUME_SIZE         INTEGER      NOT NULL, -- 볼륨 용량
	VOLUME_MOUNT_PATH   VARCHAR(500) NOT NULL  -- 볼륨 마운트 경로
);

-- 컨테이너 볼륨
ALTER TABLE CONTAINER_VOLUME
	ADD CONSTRAINT PK_CONTAINER_VOLUME -- 컨테이너 볼륨 기본키
		PRIMARY KEY (
			USERNAME,            -- 사용자아이디
			CONTAINER_ID,        -- 컨테이너 아이디
			CONTAINER_VOLUME_NO  -- 컨테이너 볼륨 번호
		);

-- 컨테이너 볼륨
ALTER TABLE CONTAINER_VOLUME
	ADD CONSTRAINT FK_CONTAINER_TO_CONTAINER_VOLUME -- 컨테이너 -> 컨테이너 볼륨
		FOREIGN KEY (
			USERNAME,     -- 사용자아이디
			CONTAINER_ID  -- 컨테이너 아이디
		)
		REFERENCES CONTAINER ( -- 컨테이너
			USERNAME,     -- 사용자아이디
			CONTAINER_ID  -- 컨테이너 아이디
		);

-- 컨테이너 볼륨
ALTER TABLE CONTAINER_VOLUME
	ADD CONSTRAINT FK_VOLUME_TO_CONTAINER_VOLUME2 -- 볼륨 -> 컨테이너 볼륨2
		FOREIGN KEY (
			VOLUME_SN -- 볼륨 일련번호
		)
		REFERENCES VOLUME ( -- 볼륨
			VOLUME_SN -- 볼륨 일련번호
		);

-- 파일
CREATE TABLE FILE (
	FILE_SN            BIGINT       NOT NULL AUTO_INCREMENT, -- 파일 일련번호
	LOGICAL_FILE_NAME  VARCHAR(100) NOT NULL, -- 논리 파일 명
	PHYSICAL_FILE_NAME VARCHAR(100) NOT NULL, -- 물리 파일 명
	FILE_EXTENSION     VARCHAR(10)  NOT NULL, -- 파일 확장자
	FILE_PATH          VARCHAR(500) NOT NULL, -- 파일 경로
	FILE_SIZE          BIGINT      NOT NULL, -- 파일 용량
	CREATED_AT         DATETIME     NOT NULL, -- 등록일시
	CREATED_BY         VARCHAR(20)  NOT NULL  -- 등록사용자
);

-- 파일
ALTER TABLE FILE
	ADD CONSTRAINT PK_FILE -- 파일 기본키
		PRIMARY KEY (
			FILE_SN -- 파일 일련번호
		);

-- 사용자 GPU
CREATE TABLE USER_GPU (
	USERNAME       VARCHAR(20) NOT NULL, -- 사용자아이디
	GPU_SN         BIGINT      NOT NULL, -- GPU 일련번호
	GROUP_QUOTA_QUANTITY INTEGER(4)  NULL     DEFAULT 0, -- 그룹 할당 수량
	QUOTA_QUANTITY INTEGER(4)  NOT NULL DEFAULT 0, -- 할당 수량
	CREATED_AT     DATETIME    NULL,     -- 등록일시
	CREATED_BY     VARCHAR(20) NULL      -- 등록사용자
);

-- 사용자 GPU
ALTER TABLE USER_GPU
	ADD CONSTRAINT PK_USER_GPU -- 사용자 GPU 기본키
		PRIMARY KEY (
			USERNAME, -- 사용자아이디
			GPU_SN    -- GPU 일련번호
		);

-- 사용자 GPU
ALTER TABLE USER_GPU
	ADD CONSTRAINT FK_USER_TO_USER_GPU -- 사용자 -> 사용자 GPU
		FOREIGN KEY (
			USERNAME -- 사용자아이디
		)
		REFERENCES USER ( -- 사용자
			USERNAME -- 사용자아이디
		);

-- 사용자 GPU
ALTER TABLE USER_GPU
	ADD CONSTRAINT FK_GPU_TO_USER_GPU -- GPU -> 사용자 GPU
		FOREIGN KEY (
			GPU_SN -- GPU 일련번호
		)
		REFERENCES GPU ( -- GPU
			GPU_SN -- GPU 일련번호
		);

-- 로그인 이력
CREATE TABLE LOGIN_HISTORY (
	LOGIN_HISTORY_SN   BIGINT      NOT NULL, -- 로그인 이력 일련번호
	USERNAME           VARCHAR(20) NOT NULL, -- 사용자아이디
	LOGIN_HISTORY_CODE VARCHAR(50) NULL,     -- 로그인 이력 코드
	LOGIN_IP           VARCHAR(15) NULL,     -- 로그인 아이피
	LOGIN_DATE         DATETIME    NULL      -- 로그인 일시
);

-- 로그인 이력
ALTER TABLE LOGIN_HISTORY
	ADD CONSTRAINT PK_LOGIN_HISTORY -- 로그인 이력 기본키
		PRIMARY KEY (
			LOGIN_HISTORY_SN -- 로그인 이력 일련번호
		);

ALTER TABLE LOGIN_HISTORY
	MODIFY COLUMN LOGIN_HISTORY_SN BIGINT NOT NULL AUTO_INCREMENT;

-- 로그인 이력
ALTER TABLE LOGIN_HISTORY
	ADD CONSTRAINT FK_USER_TO_LOGIN_HISTORY -- 사용자 -> 로그인 이력
		FOREIGN KEY (
			USERNAME -- 사용자아이디
		)
		REFERENCES USER ( -- 사용자
			USERNAME -- 사용자아이디
		);


-- 커스텀이미지
CREATE TABLE CUSTOM_IMAGE (
	CUSTOM_IMAGE_SN   BIGINT        NOT NULL, -- 커스텀 이미지 일련번호
	USERNAME          VARCHAR(20)   NOT NULL, -- 사용자아이디
	CUSTOM_IMAGE_NAME VARCHAR(100)  NOT NULL, -- 커스텀 이미지 명
	CUSTOM_IMAGE      VARCHAR(500)  NOT NULL, -- 커스텀 이미지
	CUSTOM_IMAGE_ID   VARCHAR(100)   NOT NULL, -- 커스텀 이미지 아이디
	CUSTOM_IMAGE_SIZE BIGINT        NOT NULL DEFAULT 0, -- 커스텀 이미지 용량
	DESCRIPTION       VARCHAR(100)  NULL,     -- 설명
	COMMAND           VARCHAR(500)  NULL,     -- 명령어
	STATUS_CODE       VARCHAR(50)   NOT NULL,     -- 상태 코드
	GROUP_TARGET_USER VARCHAR(1000) NULL,     -- 그룹 대상 사용자
	POD_FORMAT_SN     BIGINT(20) NOT NULL,    -- 파드 구성 일련번호
	DATA_COPY_YN      BOOLEAN       NULL     DEFAULT 0, -- 데이터 복사 여부
	BASE_PATH         VARCHAR(500)  NOT NULL, -- 기초 경로
	CREATED_AT        DATETIME      NULL,     -- 등록일시
	CREATED_BY        VARCHAR(20)   NULL,     -- 등록사용자
	UPDATED_AT        DATETIME      NULL,     -- 수정일시
	UPDATED_BY        VARCHAR(20)   NULL      -- 수정사용자
);

-- 커스텀이미지
ALTER TABLE CUSTOM_IMAGE
	ADD CONSTRAINT PK_CUSTOM_IMAGE -- 커스텀이미지 기본키
		PRIMARY KEY (
			CUSTOM_IMAGE_SN -- 커스텀 이미지 일련번호
		);

ALTER TABLE cheetah.CUSTOM_IMAGE
	MODIFY COLUMN CUSTOM_IMAGE_SN BIGINT NOT NULL AUTO_INCREMENT;

-- 커스텀이미지
ALTER TABLE CUSTOM_IMAGE
	ADD CONSTRAINT FK_USER_TO_CUSTOM_IMAGE -- 사용자 -> 커스텀이미지
		FOREIGN KEY (
			USERNAME -- 사용자아이디
		)
		REFERENCES USER ( -- 사용자
			USERNAME -- 사용자아이디
		);



-- 프로젝트
CREATE TABLE cheetah.PROJECT (
	PROJECT_ID         VARCHAR(20)   NOT NULL, -- 프로젝트 아이디
	USERNAME           VARCHAR(20)   NOT NULL, -- 사용자아이디
	PROJECT_NAME       VARCHAR(100)  NOT NULL, -- 프로젝트 명
	DESCRIPTION        VARCHAR(500)  NULL,     -- 설명
	OUTPUT_PATH        VARCHAR(100)  NOT NULL, -- 결과물 경로
	GROUP_TARGET_USER  VARCHAR(1000) NULL,     -- 그룹 대상 사용자
	STATUS_CODE        VARCHAR(50)   NOT NULL, -- 상태 코드
	TENSORBOARD_STATUS VARCHAR(50)   NULL,     -- 텐서보드 상태
	TENSORBOARD_IP     VARCHAR(15)   NULL,     -- 텐서보드 아이피
	TENSORBOARD_PORT   INTEGER(5)    NULL,     -- 텐서보드 포트
	BASE_PATH          VARCHAR(500)  NULL,     -- 기초 경로
	CREATED_AT         DATETIME      NULL,     -- 등록일시
	CREATED_BY         VARCHAR(20)   NULL,     -- 등록사용자
	UPDATED_AT         DATETIME      NULL,     -- 수정일시
	UPDATED_BY         VARCHAR(20)   NULL      -- 수정사용자
);

-- 프로젝트
ALTER TABLE cheetah.PROJECT
	ADD CONSTRAINT PK_PROJECT -- 프로젝트 기본키
		PRIMARY KEY (
			PROJECT_ID -- 프로젝트 아이디
		);

-- 프로젝트
ALTER TABLE cheetah.PROJECT
	ADD CONSTRAINT FK_USER_TO_PROJECT -- 사용자 -> 프로젝트
		FOREIGN KEY (
			USERNAME -- 사용자아이디
		)
		REFERENCES cheetah.USER ( -- 사용자
			USERNAME -- 사용자아이디
		);



-- 작업
CREATE TABLE cheetah.JOB (
	PROJECT_ID           VARCHAR(20)  NOT NULL, -- 프로젝트 아이디
	JOB_ID               VARCHAR(20)  NOT NULL, -- 작업 아이디
	JOB_NAME             VARCHAR(100) NOT NULL, -- 작업 명
	DESCRIPTION          VARCHAR(500) NULL,     -- 설명
	GPU_SN               BIGINT       NOT NULL, -- GPU 일련번호
	GPU_QUANTITY         INTEGER(4)   NOT NULL DEFAULT 0, -- GPU 수량
	IMAGE_TYPE_CODE      VARCHAR(50)  NOT NULL, -- 이미지 구분 코드
	POD_FORMAT_SN        BIGINT       NULL,     -- 파드 구성 일련번호
	CUSTOM_IMAGE_SN      BIGINT       NULL,     -- 커스텀 이미지 일련번호
	COMMAND              VARCHAR(500) NOT NULL, -- 명령어
	SCHEDULE_START_DATE  DATETIME     NOT NULL, -- 스케쥴 시작 일시
	SCHEDULE_END_DATE    DATETIME     NULL,     -- 스케쥴 종료 일시
	EXECUTION_START_DATE DATETIME     NULL,     -- 실행 시작 일시
	EXECUTION_END_DATE   DATETIME     NULL,     -- 실행 종료 일시
	NODE_SN              BIGINT       NULL,     -- 노드 일련번호
	STATUS_CODE          VARCHAR(50)  NOT NULL, -- 상태 코드
	CREATED_AT           DATETIME     NOT NULL, -- 등록일시
	CREATED_BY           VARCHAR(20)  NOT NULL, -- 등록사용자
	UPDATED_AT           DATETIME     NULL,     -- 수정일시
	UPDATED_BY           VARCHAR(20)  NULL      -- 수정사용자
);

-- 작업
ALTER TABLE cheetah.JOB
	ADD CONSTRAINT PK_JOB -- 작업 기본키
		PRIMARY KEY (
			PROJECT_ID, -- 프로젝트 아이디
			JOB_ID      -- 작업 아이디
		);

-- 작업
ALTER TABLE cheetah.JOB
	ADD CONSTRAINT FK_PROJECT_TO_JOB -- 프로젝트 -> 작업
		FOREIGN KEY (
			PROJECT_ID -- 프로젝트 아이디
		)
		REFERENCES cheetah.PROJECT ( -- 프로젝트
			PROJECT_ID -- 프로젝트 아이디
		);

-- 작업
ALTER TABLE cheetah.JOB
	ADD CONSTRAINT FK_GPU_TO_JOB -- GPU -> 작업
		FOREIGN KEY (
			GPU_SN -- GPU 일련번호
		)
		REFERENCES cheetah.GPU ( -- GPU
			GPU_SN -- GPU 일련번호
		);

-- 작업
ALTER TABLE cheetah.JOB
	ADD CONSTRAINT FK_POD_FORMAT_TO_JOB -- 파드 구성 -> 작업
		FOREIGN KEY (
			POD_FORMAT_SN -- 파드 구성 일련번호
		)
		REFERENCES cheetah.POD_FORMAT ( -- 파드 구성
			POD_FORMAT_SN -- 파드 구성 일련번호
		);

-- 작업
ALTER TABLE cheetah.JOB
	ADD CONSTRAINT FK_NODE_TO_JOB -- 노드 -> 작업
		FOREIGN KEY (
			NODE_SN -- 노드 일련번호
		)
		REFERENCES cheetah.NODE ( -- 노드
			NODE_SN -- 노드 일련번호
		);

-- 작업
ALTER TABLE cheetah.JOB
	ADD CONSTRAINT FK_CUSTOM_IMAGE_TO_JOB -- 커스텀이미지 -> 작업
		FOREIGN KEY (
			CUSTOM_IMAGE_SN -- 커스텀 이미지 일련번호
		)
		REFERENCES cheetah.CUSTOM_IMAGE ( -- 커스텀이미지
			CUSTOM_IMAGE_SN -- 커스텀 이미지 일련번호
		);

-- 작업 볼륨
CREATE TABLE cheetah.JOB_VOLUME (
	PROJECT_ID        VARCHAR(20)  NOT NULL, -- 프로젝트 아이디
	JOB_ID            VARCHAR(20)  NOT NULL, -- 작업 아이디
	JOB_VOLUME_NO     INTEGER(3)   NOT NULL DEFAULT 0, -- 작업 볼륨 번호
	CLOUD_TYPE        VARCHAR(50)  NOT NULL, -- 클라우드 구분
	VOLUME_SN         BIGINT       NULL,     -- 볼륨 일련번호
	VOLUME_ID         VARCHAR(100) NULL,     -- 볼륨 아이디
	VOLUME_SIZE       INTEGER      NOT NULL, -- 볼륨 용량
	VOLUME_MOUNT_PATH VARCHAR(500) NOT NULL  -- 볼륨 마운트 경로
);

-- 작업 볼륨
ALTER TABLE cheetah.JOB_VOLUME
	ADD CONSTRAINT PK_JOB_VOLUME -- 작업 볼륨 기본키
		PRIMARY KEY (
			PROJECT_ID,    -- 프로젝트 아이디
			JOB_ID,        -- 작업 아이디
			JOB_VOLUME_NO  -- 작업 볼륨 번호
		);

-- 작업 볼륨
ALTER TABLE cheetah.JOB_VOLUME
	ADD CONSTRAINT FK_JOB_TO_JOB_VOLUME -- 작업 -> 작업 볼륨
		FOREIGN KEY (
			PROJECT_ID, -- 프로젝트 아이디
			JOB_ID      -- 작업 아이디
		)
		REFERENCES cheetah.JOB ( -- 작업
			PROJECT_ID, -- 프로젝트 아이디
			JOB_ID      -- 작업 아이디
		);

-- 작업 볼륨
ALTER TABLE cheetah.JOB_VOLUME
	ADD CONSTRAINT FK_VOLUME_TO_JOB_VOLUME -- 볼륨 -> 작업 볼륨
		FOREIGN KEY (
			VOLUME_SN -- 볼륨 일련번호
		)
		REFERENCES cheetah.VOLUME ( -- 볼륨
			VOLUME_SN -- 볼륨 일련번호
		);


-- 게시물 파일
CREATE TABLE POST_FILE (
                           POST_SN BIGINT NOT NULL COMMENT '게시물 일련번호', -- 게시물 일련번호
                           FILE_SN BIGINT NOT NULL COMMENT '파일 일련번호' -- 파일 일련번호
) COMMENT '게시물 파일';

ALTER TABLE POST_FILE
    ADD CONSTRAINT PK_POST_FILE -- 게시물 파일 기본키
        PRIMARY KEY (
                     POST_SN, -- 게시물 일련번호
                     FILE_SN  -- 파일 일련번호
            );


INSERT INTO USER (USERNAME, PARENT_USERNAME, GROUP_NAME, GROUP_TYPE_CODE, GROUP_INVITE_KEY, NAME, PASSWORD, PHONE_NO, EMAIL, CONFIRM_YN, CREATED_AT, CREATED_BY, UPDATED_AT, UPDATED_BY, SLACK_HOOK_URL, SLACK_CHANNEL, ORGANIZATION_NAME, TEAM_NAME, GROUP_IMAGE_FILE_SN, USER_IMAGE_FILE_SN)
VALUES ('admin', null, null, null, null, '시스템관리자', '$2a$10$cK1QZgHGjXHujox1iFbRqOXGEjBMB4IlWwWQ/w1z0zNmVEwD8hFSW', '010-1111-2222', 'cheetah@n3ncloud.co.kr', 1, sysdate(), 'admin', sysdate(), 'admin', null, null, 'n3ncloud', 'cloud', null, null);
INSERT INTO USER_ROLE VALUES ('admin', 'SYSTEM_ADMIN');


INSERT INTO CODE_GROUP VALUES ('UR', 'USER ROLE');
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('UR', 'SYSTEM_ADMIN', '시스템관리자', '시스템관리자', 1, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('UR', 'GROUP_ADMIN', '그룹관리자', '그룹관리자', 2, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('UR', 'GROUP_USER', '그룹사용자', '그룹사용자', 3, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('UR', 'GENERAL_USER', '일반사용자', '일반사용자', 4, 1);
INSERT INTO code (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('UR', 'EVENT_ADMIN', '이벤트관리자', '이벤트관리자', 5, 1);
INSERT INTO code (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('UR', 'HACKATHON_USER', '해커톤사용자', '해커톤사용자', 6, 1);
INSERT INTO code (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('UR', 'IDEATHON_USER', '아이디어톤사용자', '아이디어톤사용자', 7, 1);


INSERT INTO CODE_GROUP VALUES ('GT', 'GROUP TYPE');
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('GT', 'SCHOOL', '학교', '학교', 1, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('GT', 'COMPANY', '회사', '회사', 2, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('GT', 'ORGANIZATION', '단체', '단체', 3, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('GT', 'ETC', '기타', '기타', 4, 1);

INSERT INTO CODE_GROUP VALUES ('NS', 'NODE STATUS');
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('NS', 'READY', 'READY', 'READY', 1, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('NS', 'NOT_READY', 'NOT READY', 'NOT READY', 2, 1);


INSERT INTO CODE_GROUP VALUES ('CT', 'CREDIT TYPE');
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CT', 'WELCOME_GROUP_ADMIN', '그룹관리자 환영크레딧', '그룹관리자 환영크레딧', 1, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CT', 'WELCOME_GENERAL_USER', '일반사용자 환영크레딧', '일반사용자 환영크레딧', 2, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CT', 'BONUS', '보너스 크레딧', '보너스 크레딧', 3, 1);

INSERT INTO CODE_GROUP VALUES ('CE', 'CREDIT EXPIRY TYPE');
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CE', 'NONE', '없음', '없음', 1, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CE', 'YEAR', '년단위', '년단위', 2, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CE', 'MONTH', '월단위', '월단위', 3, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CE', 'WEEK', '주단위', '주단위', 4, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CE', 'DAY', '일단위', '일단위', 5, 1);

INSERT INTO CODE_GROUP VALUES ('CS', 'CONTAINER STATUS');
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CS', 'REQUEST', '요청', '요청', 1, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CS', 'CONFIRM', '승인', '승인', 2, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CS', 'CREATE', '생성중', '생성중', 3, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CS', 'START', '시작', '시작', 4, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CS', 'STOP', '중지', '중지', 5, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CS', 'FAIL', '실패', '실패', 6, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CS', 'RETURN', '반납', '반납', 7, 1);


INSERT INTO CODE_GROUP VALUES ('BT', 'BOARD TYPE');
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('BT', 'QNA', 'QNA', 'QNA', 1, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('BT', 'NOTICE', 'NOTICE', 'NOTICE', 2, 1);


INSERT INTO CODE_GROUP VALUES ('OG', 'POD OPTION GROUP TYPE');
DELETE FROM CODE WHERE CODE_GROUP_ID = 'OG';
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('OG', 'REQUIRED', 'REQUIRED', 'REQUIRED', 1, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('OG', 'LIBRARY', 'LIBRARY', 'LIBRARY', 2, 1);


INSERT INTO CODE_GROUP VALUES ('CM', 'CHARGING METHOD');
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CM', 'MINUTELY', '분단위', '분단위', 1, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CM', 'HOURLY', '시간단위', '시간단위', 2, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CM', 'DAILY', '일단위', '일단위', 3, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CM', 'MONTHLY', '월단위', '월단위', 4, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CM', 'MONTHLY_CONTRACT_1', '1달 약정', '1달 약정', 5, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CM', 'MONTHLY_CONTRACT_3', '3달 약정', '3달 약정', 6, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CM', 'MONTHLY_CONTRACT_6', '6달 약정', '6달 약정', 7, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CM', 'MONTHLY_CONTRACT_12', '1년 약정', '1년 약정', 8, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CM', 'MONTHLY_CONTRACT_24', '2년 약정', '2년 약정', 9, 1);


INSERT INTO CODE_GROUP VALUES ('CL', 'CLOUD TYPE');
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CL', 'ON_PREMISE', 'ON_PREMISE', 'ON_PREMISE', 1, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CL', 'AWS', 'AWS', 'AWS', 2, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CL', 'GCP', 'GCP', 'GCP', 3, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('CL', 'TENCENT', 'TENCENT', 'TENCENT', 4, 1);


INSERT INTO CODE_GROUP VALUES ('RT', 'RESOURCE TYPE');
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('RT', 'GPU', 'GPU', 'GPU', 1, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('RT', 'CPU', 'CPU', 'CPU', 2, 1);


INSERT INTO CODE_GROUP VALUES ('LH', 'LOGIN HISTORY');
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('LH', 'SUCCESS', 'SUCCESS', 'SUCCESS', 1, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('LH', 'FAIL_WRONG_PASSWORD', 'FAIL_WRONG_PASSWORD', 'FAIL_WRONG_PASSWORD', 2, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('LH', 'FAIL_NOT_CONFIRMED', 'FAIL_NOT_CONFIRMED', 'FAIL_NOT_CONFIRMED', 3, 1);


INSERT INTO CODE_GROUP VALUES ('ST', 'SRC TYPE');
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('ST', 'FILE', 'FILE', 'FILE', 1, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('ST', 'GIT', 'GIT', 'GIT', 2, 1);

INSERT INTO CODE_GROUP VALUES ('IT', 'IMAGE TYPE');
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('IT', 'GENERAL', 'GENERAL', 'GENERAL', 1, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('IT', 'GENERAL_LIST', 'GENERAL_LIST', 'GENERAL_LIST', 2, 1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('IT', 'CUSTOM', 'CUSTOM', 'CUSTOM', 3, 1);

-- 커스텀이미지 코드
INSERT INTO CODE_GROUP (CODE_GROUP_ID, CODE_GROUP_NAME) VALUES ('IS','CUSTOM IMAGE STATUS');
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('IS','COMMIT','커밋','커밋',1,1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('IS','UPLOADING','업로드','업로드',2,1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('IS','ERROR','에러','에러',3,1);
INSERT INTO CODE (CODE_GROUP_ID, CODE, CODE_NAME, CODE_DESCRIPTION, ODR, ACTIVE_YN) VALUES ('IS','AVAILABLE','정상','정상',4,1);

-- 크레딧
INSERT INTO CREDIT VALUES ('WELCOME_CREDIT_FOR_GROUP', '그룹 회원가입 크레딧', 'WELCOME_GROUP_ADMIN', 300000, 'NONE', 1, sysdate(), 'admin', null, null);
INSERT INTO CREDIT VALUES ('WELCOME_CREDIT_FOR_USER', '사용자 회원가입 크레딧', 'WELCOME_GENERAL_USER', 100000, 'YEAR', 1, sysdate(), 'admin', null, null);
INSERT INTO CREDIT VALUES ('BONUS_CREDIT_1_YEAR', '보너스 크레딧(1년)', 'BONUS', 5000, 'YEAR', 1, sysdate(), 'admin', null, null);


INSERT INTO POD_OPTION_GROUP VALUES ('OS', '운영체제', 'REQUIRED', 1, 1, sysdate(), 'admin', sysdate(), 'admin');
INSERT INTO POD_OPTION_GROUP VALUES ('JUPYTER', 'Jupyter', 'REQUIRED', 2, 1, sysdate(), 'admin', sysdate(), 'admin');
INSERT INTO POD_OPTION_GROUP VALUES ('TENSORFLOW', 'Tensorflow', 'LIBRARY', 3, 1, sysdate(), 'admin', sysdate(), 'admin');
INSERT INTO POD_OPTION_GROUP VALUES ('KERAS', 'Keras', 'LIBRARY', 4, 1, sysdate(), 'admin', sysdate(), 'admin');
INSERT INTO POD_OPTION_GROUP VALUES ('PYTORCH', 'Pytorch', 'LIBRARY', 5, 1, sysdate(), 'admin', sysdate(), 'admin');
INSERT INTO POD_OPTION_GROUP VALUES ('R', 'R', 'LIBRARY', 6, 1, sysdate(), 'admin', sysdate(), 'admin');


INSERT INTO POD_OPTION VALUES (1, 'OS', 'UBUNTU16_04_CUDA9_0', 'ubuntu16.04 cuda9.0', 1, 1, sysdate(), 'admin', sysdate(), 'admin');
INSERT INTO POD_OPTION VALUES (2, 'OS', 'UBUNTU16_04_CUDA10_0', 'ubuntu16.04 cuda10.0', 2, 1, sysdate(), 'admin', sysdate(), 'admin');
INSERT INTO POD_OPTION VALUES (15, 'OS', 'UBUNTU16_04', 'ubuntu16.04', 3, 1, sysdate(), 'admin', sysdate(), 'admin');


INSERT INTO POD_OPTION VALUES (3, 'JUPYTER', 'V5_7_5', 'v5.7.5', 1, 1, sysdate(), 'admin', sysdate(), 'admin');

INSERT INTO POD_OPTION VALUES (4, 'TENSORFLOW', 'NONE', '없음', 1, 1, sysdate(), 'admin', sysdate(), 'admin');
INSERT INTO POD_OPTION VALUES (5, 'TENSORFLOW', 'V1_5', 'v1.5', 2, 1, sysdate(), 'admin', sysdate(), 'admin');
INSERT INTO POD_OPTION VALUES (6, 'TENSORFLOW', 'V1_12', 'v1.12', 3, 1, sysdate(), 'admin', sysdate(), 'admin');


INSERT INTO POD_OPTION VALUES (7, 'KERAS', 'NONE', '없음', 1, 1, sysdate(), 'admin', sysdate(), 'admin');
INSERT INTO POD_OPTION VALUES (8, 'KERAS', 'V2_0', 'v2.0', 2, 1, sysdate(), 'admin', sysdate(), 'admin');
INSERT INTO POD_OPTION VALUES (9, 'KERAS', 'V2_2', 'v2.2', 3, 1, sysdate(), 'admin', sysdate(), 'admin');

INSERT INTO POD_OPTION VALUES (10, 'PYTORCH', 'NONE', '없음', 1, 1, sysdate(), 'admin', sysdate(), 'admin');
INSERT INTO POD_OPTION VALUES (11, 'PYTORCH', 'V0_4', 'v0.4', 2, 1, sysdate(), 'admin', sysdate(), 'admin');
INSERT INTO POD_OPTION VALUES (12, 'PYTORCH', 'V1_0', 'v1.0', 3, 1, sysdate(), 'admin', sysdate(), 'admin');

INSERT INTO POD_OPTION VALUES (13, 'R', 'NONE', '없음', 1, 1, sysdate(), 'admin', sysdate(), 'admin');
INSERT INTO POD_OPTION VALUES (14, 'R', 'V_3_6_1', 'v3.6.1', 2, 1, sysdate(), 'admin', sysdate(), 'admin');


INSERT INTO GPU (GPU_SN, GPU_NAME, MANUFACTURER_NAME, SPEC, MONTHLY_AMOUNT, USE_YN, CREATED_AT, CREATED_BY, UPDATED_AT, UPDATED_BY, GPU_LABEL, CLOUD_TYPE, MINUTELY_AMOUNT, HOURLY_AMOUNT, DAILY_AMOUNT, MONTHLY_CONTRACT_AMOUNT, MONTHLY_CONTRACT_AMOUNT_3, MONTHLY_CONTRACT_AMOUNT_6, MONTHLY_CONTRACT_AMOUNT_12, MONTHLY_CONTRACT_AMOUNT_24, QUANTITY_FORMAT, RESOURCE_TYPE, SHARED_YN, GPU_MEMORY, CPU_CORE, SYSTEM_MEMORY)
VALUES (1, 'NVIDIA GeForce GTX 1080Ti', 'NVIDIA', 'Pascal / 3,584 CUDA Cores / 11GB GDDR5X / 1,582 MHz', 0, 1, sysdate(), 'admin', sysdate(), 'admin', 'nvidia-gtx-1080ti', 'ON_PREMISE', 0, 0, 0, 0, 0, 0, 0, 0, '1, 2, 3, 4', 'GPU', 0, 12, 0, 0);
INSERT INTO GPU (GPU_SN, GPU_NAME, MANUFACTURER_NAME, SPEC, MONTHLY_AMOUNT, USE_YN, CREATED_AT, CREATED_BY, UPDATED_AT, UPDATED_BY, GPU_LABEL, CLOUD_TYPE, MINUTELY_AMOUNT, HOURLY_AMOUNT, DAILY_AMOUNT, MONTHLY_CONTRACT_AMOUNT, MONTHLY_CONTRACT_AMOUNT_3, MONTHLY_CONTRACT_AMOUNT_6, MONTHLY_CONTRACT_AMOUNT_12, MONTHLY_CONTRACT_AMOUNT_24, QUANTITY_FORMAT, RESOURCE_TYPE, SHARED_YN, GPU_MEMORY, CPU_CORE, SYSTEM_MEMORY)
VALUES (2, 'NVIDIA GeForce RTX 2080Ti', 'NVIDIA', 'Pascal / 4,352 CUDA Cores / 11GB GDDR6 / 1,635 MHz', 0, 1, sysdate(), 'admin', sysdate(), 'admin', 'nvidia-rtx-2080ti', 'ON_PREMISE', 0, 0, 0, 0, 0, 0, 0, 0, '1, 2, 3, 4', 'GPU', 0, 12, 0, 0);
INSERT INTO GPU (GPU_SN, GPU_NAME, MANUFACTURER_NAME, SPEC, MONTHLY_AMOUNT, USE_YN, CREATED_AT, CREATED_BY, UPDATED_AT, UPDATED_BY, GPU_LABEL, CLOUD_TYPE, MINUTELY_AMOUNT, HOURLY_AMOUNT, DAILY_AMOUNT, MONTHLY_CONTRACT_AMOUNT, MONTHLY_CONTRACT_AMOUNT_3, MONTHLY_CONTRACT_AMOUNT_6, MONTHLY_CONTRACT_AMOUNT_12, MONTHLY_CONTRACT_AMOUNT_24, QUANTITY_FORMAT, RESOURCE_TYPE, SHARED_YN, GPU_MEMORY, CPU_CORE, SYSTEM_MEMORY)
VALUES (3, 'cpu-2-core', 'n3ncloud', 'cpu 2 core', 0, 1, sysdate(), 'admin', sysdate(), 'admin', 'cpu-2-core', 'ON_PREMISE', 0, 0, 0, 0, 0, 0, 0, 0, '1', 'CPU', 0, 0, 2, 4);

INSERT INTO POD_FORMAT (POD_FORMAT_SN, POD_FORMAT_NAME, POD_FORMAT_DESCRIPTION, IMAGE, USE_YN, ODR, CREATED_AT, CREATED_BY, UPDATED_AT, UPDATED_BY, CAPACITY, COMMAND, POD_OPTION_HINT) VALUES (1, 'ubuntu16.04-jupyter5.7.5-tensorflow1.12', 'Jupyter Tensorflow', 'cheetah-jupyter-tensorflow:ubuntu16.04-jupyter5.7.5-tensorflow1.12', 1, 2, sysdate(), 'admin', sysdate(), 'admin', '200Gi', 'python3.6 /usr/src/app/websocketserver.py & /usr/sbin/sshd -D & echo ''jovyan:{password}'' | chpasswd & mkdir .local; chown 1000:1000 -R .local; chown 1000:1000 .; jupyter notebook --generate-config; chown 1000:1000 -R .jupyter; start-notebook.sh --NotebookApp.token=''{token}''', '{JUPYTER=3, KERAS=7, OS=2, PYTORCH=10, R=13, TENSORFLOW=6}');
INSERT INTO POD_FORMAT (POD_FORMAT_SN, POD_FORMAT_NAME, POD_FORMAT_DESCRIPTION, IMAGE, USE_YN, ODR, CREATED_AT, CREATED_BY, UPDATED_AT, UPDATED_BY, CAPACITY, COMMAND, POD_OPTION_HINT) VALUES (2, 'ubuntu16.04-jupyter5.7.5', 'Jupyter', 'cheetah-jupyter:ubuntu16.04-jupyter5.7.5', 1, 1, sysdate(), 'admin', sysdate(), 'admin', '200Gi', 'python3.6 /usr/src/app/websocketserver.py & /usr/sbin/sshd -D & echo ''jovyan:{password}'' | chpasswd & mkdir .local; chown 1000:1000 -R .local; chown 1000:1000 .; jupyter notebook --generate-config; chown 1000:1000 -R .jupyter; start-notebook.sh --NotebookApp.token=''{token}''', '{JUPYTER=3, KERAS=7, OS=2, PYTORCH=10, R=13, TENSORFLOW=4}');
INSERT INTO POD_FORMAT (POD_FORMAT_SN, POD_FORMAT_NAME, POD_FORMAT_DESCRIPTION, IMAGE, USE_YN, ODR, CREATED_AT, CREATED_BY, UPDATED_AT, UPDATED_BY, CAPACITY, COMMAND, POD_OPTION_HINT) VALUES (3, 'ubuntu16.04-jupyter5.7.5-tensorflow1.12-keras2.2', 'Jupyter Tensorflow Keras', 'cheetah-jupyter-tensorflow-keras:ubuntu16.04-jupyter5.7.5-tensorflow1.12-keras2.2', 1, 3, sysdate(), 'admin', sysdate(), 'admin', '200Gi', 'python3.6 /usr/src/app/websocketserver.py & /usr/sbin/sshd -D & echo ''jovyan:{password}'' | chpasswd & mkdir .local; chown 1000:1000 -R .local; chown 1000:1000 .; jupyter notebook --generate-config; chown 1000:1000 -R .jupyter; start-notebook.sh --NotebookApp.token=''{token}''', '{JUPYTER=3, KERAS=9, OS=2, PYTORCH=10, R=13, TENSORFLOW=6}');
INSERT INTO POD_FORMAT (POD_FORMAT_SN, POD_FORMAT_NAME, POD_FORMAT_DESCRIPTION, IMAGE, USE_YN, ODR, CREATED_AT, CREATED_BY, UPDATED_AT, UPDATED_BY, CAPACITY, COMMAND, POD_OPTION_HINT) VALUES (4, 'ubuntu16.04-jupyter5.7.5-tensorflow1.12-pytorch1.0', 'Jupyter Tensorflow Pytorch', 'cheetah-jupyter-tensorflow-pytorch:ubuntu16.04-jupyter5.7.5-tensorflow1.12-pytorch1.0', 1, 4, sysdate(), 'admin', sysdate(), 'admin', '200Gi', 'python3.6 /usr/src/app/websocketserver.py & /usr/sbin/sshd -D & echo ''jovyan:{password}'' | chpasswd & mkdir .local; chown 1000:1000 -R .local; chown 1000:1000 .; jupyter notebook --generate-config; chown 1000:1000 -R .jupyter; start-notebook.sh --NotebookApp.token=''{token}''', '{JUPYTER=3, KERAS=7, OS=2, PYTORCH=12, R=13, TENSORFLOW=6}');
INSERT INTO POD_FORMAT (POD_FORMAT_SN, POD_FORMAT_NAME, POD_FORMAT_DESCRIPTION, IMAGE, USE_YN, ODR, CREATED_AT, CREATED_BY, UPDATED_AT, UPDATED_BY, CAPACITY, COMMAND, POD_OPTION_HINT) VALUES (8, 'ubuntu16.04-jupyter5.7.5-tensorflow1.12-r3.6.1', 'jupyter/tensorflow/r', 'cheetah-cpu-jupyter-r:ubuntu16.04-jupyter5.7.5-r3.6.1', 1, 5, '2019-08-08 07:37:01', 'admin', '2019-08-08 16:37:39', 'admin', '200Gi', 'python3.6 /usr/src/app/websocketserver.py & /usr/sbin/sshd -D & echo ''jovyan:{password}'' | chpasswd & mkdir .local; chown 1000:1000 -R .local; chown 1000:1000 .; jupyter notebook --generate-config; chown 1000:1000 -R .jupyter; start-notebook.sh --NotebookApp.token=''{token}''', '{JUPYTER=3, KERAS=7, OS=2, PYTORCH=10, R=14, TENSORFLOW=4}');


INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (1, 2, 'OS', 'UBUNTU16_04_CUDA10_0', sysdate());
INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (1, 3, 'JUPYTER', 'V5_7_5', sysdate());
INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (1, 6, 'TENSORFLOW', 'V1_12', sysdate());
INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (1, 7, 'KERAS', 'NONE', sysdate());
INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (1, 10, 'PYTORCH', 'NONE', sysdate());
INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (1, 13, 'R', 'NONE', sysdate());

INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (2, 2, 'OS', 'UBUNTU16_04_CUDA10_0', sysdate());
INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (2, 3, 'JUPYTER', 'V5_7_5', sysdate());
INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (2, 4, 'TENSORFLOW', 'NONE', sysdate());
INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (2, 7, 'KERAS', 'NONE', sysdate());
INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (2, 10, 'PYTORCH', 'NONE', sysdate());
INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (2, 13, 'R', 'NONE', sysdate());

INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (3, 2, 'OS', 'UBUNTU16_04_CUDA10_0', sysdate());
INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (3, 3, 'JUPYTER', 'V5_7_5', sysdate());
INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (3, 6, 'TENSORFLOW', 'V1_12', sysdate());
INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (3, 9, 'KERAS', 'V2_2', sysdate());
INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (3, 10, 'PYTORCH', 'NONE', sysdate());
INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (3, 13, 'R', 'NONE', sysdate());

INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (4, 2, 'OS', 'UBUNTU16_04_CUDA10_0', sysdate());
INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (4, 3, 'JUPYTER', 'V5_7_5', sysdate());
INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (4, 6, 'TENSORFLOW', 'V1_12', sysdate());
INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (4, 7, 'KERAS', 'NONE', sysdate());
INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (4, 12, 'PYTORCH', 'V1_0', sysdate());
INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (4, 13, 'R', 'NONE', sysdate());

INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (8, 2, 'OS', 'UBUNTU16_04_CUDA10_0', sysdate());
INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (8, 3, 'JUPYTER', 'V5_7_5', sysdate());
INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (8, 4, 'TENSORFLOW', 'NONE', sysdate());
INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (8, 7, 'KERAS', 'NONE', sysdate());
INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (8, 10, 'PYTORCH', 'NONE', sysdate());
INSERT INTO POD_FORMAT_OPTION (POD_FORMAT_SN, POD_OPTION_SN, POD_OPTION_GROUP_ID, POD_OPTION_ID, CREATED_AT) VALUES (8, 14, 'R', 'V_3_6_1', sysdate());


INSERT INTO POD_FORMAT_GPU (GPU_SN, POD_FORMAT_SN, CREATED_AT) VALUES (1, 1, sysdate());
INSERT INTO POD_FORMAT_GPU (GPU_SN, POD_FORMAT_SN, CREATED_AT) VALUES (1, 2, sysdate());
INSERT INTO POD_FORMAT_GPU (GPU_SN, POD_FORMAT_SN, CREATED_AT) VALUES (1, 3, sysdate());
INSERT INTO POD_FORMAT_GPU (GPU_SN, POD_FORMAT_SN, CREATED_AT) VALUES (1, 4, sysdate());

INSERT INTO POD_FORMAT_GPU (GPU_SN, POD_FORMAT_SN, CREATED_AT) VALUES (2, 1, sysdate());
INSERT INTO POD_FORMAT_GPU (GPU_SN, POD_FORMAT_SN, CREATED_AT) VALUES (2, 2, sysdate());
INSERT INTO POD_FORMAT_GPU (GPU_SN, POD_FORMAT_SN, CREATED_AT) VALUES (2, 3, sysdate());
INSERT INTO POD_FORMAT_GPU (GPU_SN, POD_FORMAT_SN, CREATED_AT) VALUES (2, 4, sysdate());

INSERT INTO POD_FORMAT_GPU (GPU_SN, POD_FORMAT_SN, CREATED_AT) VALUES (3, 8, sysdate());


INSERT INTO KUBERNETES_YAML (KIND, YAML) VALUES ('DEPLOYMENT', 'apiVersion: apps/v1
kind: Deployment
metadata:
  name: {label}
  namespace: {namespace}
  labels:
    app: {label}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: {label}
  template:
    metadata:
      labels:
        app: {label}
      namespace: {namespace}
    spec:
      dnsPolicy: Default
      securityContext:
        fsGroup: 0
        runAsUser: 0
      nodeSelector:
        accelerator: {gpu}
      containers:
      - name: {label}
        image: {image}
        ports:
        - name: jupyter
          containerPort: 8888
        - name: websocket
          containerPort: 8010
        - name: ssh
          containerPort: 22
        - name: tensorboard
          containerPort: 6006
        imagePullPolicy: IfNotPresent
        command: ["/bin/sh", "-c"]
        args: ["{command}"]
        env:
        - name: LD_LIBRARY_PATH
          value: /usr/lib/nvidia:/usr/lib/x86_64-linux-gnu
        resources:
          limits:
            {gpuLabel}: {gpuQuantity}
            memory: {memorySize}
          requests:
            {gpuLabel}: {gpuQuantity}
            memory: {memorySize}
        volumeMounts:
        - mountPath: /usr/local/nvidia/bin
          name: bin
        - mountPath: /usr/lib/nvidia
          name: lib
        - mountPath: /usr/lib/x86_64-linux-gnu/libcuda.so.1
          name: libcuda
        - mountPath: /home/jovyan/
          name: jupyter
      volumes:
      - name: bin
        hostPath:
          path: /usr/lib/nvidia-384/bin
      - name: lib
        hostPath:
          path: /usr/lib/nvidia-384
      - name: libcuda
        hostPath:
          path: /usr/lib/x86_64-linux-gnu/libcuda.so.1
      - name: jupyter
        persistentVolumeClaim:
            claimName: {label}');
INSERT INTO KUBERNETES_YAML (KIND, YAML) VALUES ('SERVICE', 'apiVersion: v1
kind: Service
metadata:
  labels:
    app: {label}
  name: {label}
  namespace: {namespace}
spec:
  ports:
  - name: jupyter
    port: 8888
    targetPort: 8888
  - name: websocket
    port: 8010
    targetPort: 8010
  - name: ssh
    port: 22
    targetPort: 22
  - name: tensorboard
    port: 6006
    targetPort: 6006
  selector:
    app: {label}
  type: NodePort');
INSERT INTO KUBERNETES_YAML (KIND, YAML) VALUES ('VOLUME', 'kind: PersistentVolume
apiVersion: v1
metadata:
 name: {label}
 namespace: {namespace}
spec:
 persistentVolumeReclaimPolicy: Retain
 storageClassName: {label}
 capacity:
  storage: {capacity}
 accessModes:
  - ReadWriteOnce
 hostPath:
  path: {jupyterPath}');
INSERT INTO KUBERNETES_YAML (KIND, YAML) VALUES ('VOLUME_CLAIM', 'kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: {label}
  namespace: {namespace}
spec:
  storageClassName: {label}
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
     storage: {capacity}');

INSERT INTO kubernetes_yaml (KIND, YAML) VALUES ('JOB', 'apiVersion: batch/v1
kind: Job
metadata:
  name: {label}
  namespace: {namespace}
  labels:
    app: {label}
spec:
  schedule: {schedule}
  jobTemplate:
    spec:
      template:
        metadata:
          labels:
            app: {label}
          namespace: {namespace}
        spec:
          nodeSelector:
            accelerator: {gpu}
          restartPolicy: OnFailure
          containers:
          - name: {label}
            image: {image}
            command: ["/bin/sh", "-c"]
            args: ["{command}"]
            imagePullPolicy: IfNotPresent
            lifecycle:
              postStart:
                exec:
                  command: ["/bin/sh", "-c", "curl -X POST -H ''Content-type: application/json'' --data ''{\\"hostname\\":\\"''\\"$HOSTNAME\\"''\\"}'' http://cheetah.n3ncloud.co.kr/kube/job-start/{username}/{jobId}"]
              preStop:
                exec:
                  command: ["/bin/sh", "-c", "curl -X POST -H ''Content-type: application/json'' --data ''{\\"hostname\\":\\"''\\"$HOSTNAME\\"''\\"}'' http://cheetah.n3ncloud.co.kr/kube/job-stop/{username}/{jobId}"]
            env:
            - name: LD_LIBRARY_PATH
              value: /usr/lib/nvidia:/usr/lib/x86_64-linux-gnu
            volumeMounts:
            - mountPath: /usr/local/nvidia/bin
              name: bin
            - mountPath: /usr/lib/nvidia
              name: lib
            - mountPath: /usr/lib/x86_64-linux-gnu/libcuda.so.1
              name: libcuda
            - mountPath: /home/jovyan/
              name: jupyter
            resources:
              limits:
                nvidia.com/gpu: {gpuQuantity}
              requests:
                nvidia.com/gpu: {gpuQuantity}
          volumes:
          - name: bin
            hostPath:
              path: /usr/lib/nvidia-384/bin
          - name: lib
            hostPath:
              path: /usr/lib/nvidia-384
          - name: libcuda
            hostPath:
              path: /usr/lib/x86_64-linux-gnu/libcuda.so.1
          - name: jupyter
            persistentVolumeClaim:
              claimName: {label}');
INSERT INTO kubernetes_yaml (KIND, YAML) VALUES ('DEPLOYMENT_SSH', 'apiVersion: apps/v1
kind: Deployment
metadata:
  name: {label}
  namespace: {namespace}
  labels:
    app: {label}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: {label}
  template:
    metadata:
      labels:
        app: {label}
      namespace: {namespace}
    spec:
      dnsPolicy: Default
      containers:
      - name: {label}
        image: registry.n3ncloud:5000/cheetah-ssh:ubuntu16.04
        ports:
        - name: ssh
          containerPort: 22
        imagePullPolicy: IfNotPresent
        command: ["/bin/sh", "-c"]
        args: ["echo ''jovyan:{password}'' | chpasswd & /usr/sbin/sshd -D & sleep infinity"]
        volumeMounts:
        - mountPath: /home/jovyan/{volumeName}
          name: ssh
      volumes:
      - name: ssh
        persistentVolumeClaim:
          claimName: {label}');
INSERT INTO kubernetes_yaml (KIND, YAML) VALUES ('SERVICE_SSH', 'apiVersion: v1
kind: Service
metadata:
  labels:
    app: {label}
  name: {label}
  namespace: {namespace}
spec:
  ports:
  - name: ssh
    port: 22
    targetPort: 22
  selector:
    app: {label}
  type: NodePort');