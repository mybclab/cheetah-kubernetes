###############################################################
# cheetah-base:cuda9.0-cudnn7-runtime-ubuntu16.04              #
###############################################################
FROM nvidia/cuda:9.0-cudnn7-runtime-ubuntu16.04

LABEL maintainer="CHEETAH GPU CLOUD Project <want813@mybclab.com>"


# ARG PYTHON_VERSION=3.6

RUN apt-get update
RUN apt-get install -y software-properties-common
RUN add-apt-repository -y ppa:jonathonf/python-3.6

RUN apt-get update && apt-get install -y --no-install-recommends \
         build-essential \
         cmake \
         git \
         curl \
         vim \
         wget \
         bzip2 \
         ca-certificates \
         sudo \
         locales \
         fonts-liberation \
         pkg-config \
         python3.6 \
         python3.6-dev \
         python3-pip \
         python3.6-venv \
         libmysqlclient-dev \
         # requirements for numpy
         libopenblas-base \
         python3-numpy \
         python3-scipy \
         # requirements for keras
         python3-h5py \
         python3-yaml \
         python3-pydot \
         gcc \
         libjpeg-dev \
         libpng-dev \
         ssh \
     && rm -rf /var/lib/apt/lists/*

RUN python3.6 -m pip install pip --upgrade
RUN python3.6 -m pip install --upgrade setuptools
RUN python3.6 -m pip install wheel

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen

# websocketserver
WORKDIR /usr/src/app
COPY websocketserver.py websocketserver.py

RUN python3.6 -m pip install --upgrade pip
RUN python3.6 -m pip install tornado
RUN python3.6 -m pip install terminado

EXPOSE 8010

CMD ["python3.6", "websocketserver.py"]

RUN apt-get update && apt-get install -y openssh-server
RUN mkdir /var/run/sshd
# RUN echo 'root:THEPASSWORDYOUCREATED' | chpasswd
RUN sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config

# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

EXPOSE 22
CMD ["/usr/sbin/sshd", "-D"]

# cuda, cudnn 확인
# cat /usr/local/cuda/version.txt                                                   // cuda 설치 확인
# cat /usr/include/cudnn.h | grep -E "CUDNN_MAJOR|CUDNN_MINOR|CUDNN_PATCHLEVEL"     // cuDNN 설치 확인