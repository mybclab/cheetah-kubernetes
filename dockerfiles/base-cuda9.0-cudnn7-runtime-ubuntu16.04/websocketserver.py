import tornado.web
import socket
from tornado.ioloop import IOLoop
from terminado import TermSocket, SingleTermManager


class TermSocketNew(TermSocket):

    def check_origin(self, origin):
        return True

if __name__ == '__main__':
    term_manager = SingleTermManager(shell_command=['bash'])

    handlers = [
                (r"/websocket", TermSocketNew, {'term_manager': term_manager}),
                (r"/()", tornado.web.RedirectHandler, {'url':'http://110.45.178.10/kube/webconsole/' + socket.gethostname()}),
                (r"/(.*)", tornado.web.StaticFileHandler, {'path':'.'}),
               ]
    app = tornado.web.Application(handlers)
    app.listen(8010)
    IOLoop.current().start()