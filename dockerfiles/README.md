# CHEETAH DOCKER FILE TAGS




### cheetah-base
 1. cuda10.0-cudnn7-runtime-ubuntu16.04
 2. cuda9.0-cudnn7-runtime-ubuntu16.04

### cheetah-base-cpu
 1. ubuntu16.04
 
### cheetah-sftp
 1. ubuntu16.04

### cheetah-jupyter
 1. ubuntu16.04-jupyter5.7.5
 2. ubuntu16.04-cuda9.0-jupyter5.7.5
 
### cheetah-cpu-jupyter
 1. ubuntu16.04-jupyter5.7.5
 
### cheetah-cpu-jupyter-r
 1. ubuntu16.04-jupyter5.7.5-r3.6.1
 
### cheetah-jupyter-tensorflow
 1. ubuntu16.04-jupyter5.7.5-tensorflow1.12
 2. ubuntu16.04-cuda9.0-jupyter5.7.5-tensorflow1.5

### cheetah-jupyter-tensorflow-keras
 1. ubuntu16.04-jupyter5.7.5-tensorflow1.12-keras2.2
 1. ubuntu16.04-cuda9.0-jupyter5.7.5-tensorflow1.5-keras2.0
 
### cheetah-jupyter-tensorflow-pytorch
 1. ubuntu16.04-jupyter5.7.5-tensorflow1.12-pytorch1.0